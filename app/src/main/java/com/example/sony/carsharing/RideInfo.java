package com.example.sony.carsharing;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class RideInfo extends Fragment {

    TextView rideDate, tv_email, rideTime, rideSrcTime, rideDestTime, rideDest, rideSrc, rideAvailSeats, ridePrice, rideComment, rideDistance, tv_cont_no, tv_name;
    Button call, send_sms, sms, btnBook;
    SmsManager sm, req;
    TextView tv_contact, reg_id1, info_offer_id;

    TextView facility,type,car_name,taxi_service,driver_name,driver_work_at;

    public static String MyPREFERENCE = "MyPref";
    private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1;
    EditText enter_msg;
    public static String mobileNo, userinfo_offer_id, oId, uname, from_d, to_d, email, mobile;
    int position;
    public static String regId, dregId,nm,mobile_no;
    // public static  String MyPREFERENCES="MyPref;"
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public RideInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_ride_info, container, false);

        rideDate = (TextView) view.findViewById(R.id.ride_info_date);
        rideTime = (TextView) view.findViewById(R.id.ride_info_time);
        rideSrcTime = (TextView) view.findViewById(R.id.ride_info_source_time);
        rideSrc = (TextView) view.findViewById(R.id.ride_info_source);
        rideDestTime = (TextView) view.findViewById(R.id.ride_info_destination_time);
        rideDest = (TextView) view.findViewById(R.id.ride_info_destination);
        rideAvailSeats = (TextView) view.findViewById(R.id.ride_info_avail_seats);
        ridePrice = (TextView) view.findViewById(R.id.ride_info_price);
        rideComment = (TextView) view.findViewById(R.id.ride_info_comment);
        rideDistance = (TextView) view.findViewById(R.id.ride_info_distance);
        tv_cont_no = (TextView) view.findViewById(R.id.tv_cont_no);
        tv_email = (TextView) view.findViewById(R.id.tv_email);
        info_offer_id = (TextView) view.findViewById(R.id.info_offer_id);
        tv_name = (TextView) view.findViewById(R.id.info_name);



        facility = (TextView) view.findViewById(R.id.ride_info_facility);
        type = (TextView) view.findViewById(R.id.ride_info_type);

        car_name = (TextView) view.findViewById(R.id.ride_info_car_name);

        taxi_service = (TextView) view.findViewById(R.id.ride_info_taxi_service);

        driver_name = (TextView) view.findViewById(R.id.ride_info_driver_name);

        driver_work_at = (TextView) view.findViewById(R.id.ride_info_work_at);



        reg_id1 = (TextView) view.findViewById(R.id.reg_id1);
        reg_id1.setVisibility(View.GONE);
        tv_email.setVisibility(View.GONE);
        tv_cont_no.setVisibility(View.GONE);
        info_offer_id.setVisibility(View.GONE);
        tv_name.setVisibility(View.GONE);
        call = (Button) view.findViewById(R.id.contact_person);
        sms = (Button) view.findViewById(R.id.sms);

        btnBook = (Button) view.findViewById(R.id.book);
       /* getPermissionToReadUserContacts();
*/


        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preferences = getActivity().getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                String header_uname = preferences.getString("name", LoginActivity.name);//mobile
                mobile_no=preferences.getString("mobile", LoginActivity.mobile);


                nm=tv_name.getText().toString();
       //         mobileNo=tv_cont_no.getText().toString();

                preferences = getActivity().getSharedPreferences(MyPREFERENCE, Context.MODE_PRIVATE);
                editor = preferences.edit();
                editor.putString("U_NAME",nm );
                editor.commit();

                setNotification(dregId, userinfo_offer_id, header_uname, mobile_no, header_uname);

            }
        });


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_cont_no.getText().toString()));
                try {

                    //   if(ContextCompat.checkSelfPermission(this,Manifest.pe))

                  /*  getPermissionToReadUserContacts();*/
                    startActivity(i);

                } catch (android.content.ActivityNotFoundException ex) {

                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Message Successfully Sent !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Call Not Connected ! Please Try Again.", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.contact_dialog);
                tv_contact = (TextView) dialog.findViewById(R.id.tv_contact);
                tv_contact.setText(tv_cont_no.getText().toString());
                enter_msg = (EditText) dialog.findViewById(R.id.enter_msg);
                send_sms = (Button) dialog.findViewById(R.id.send_sms);
                send_sms.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sm(v);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }


        });

        return view;
    }

    public void sm(View view) {

       /* getPermissionToReadUserContacts();*/
        sm = SmsManager.getDefault();
        // hasPermissionInManifest(getActivity(),Manifest.permission.)
        sm.sendTextMessage(tv_cont_no.getText().toString(), null, enter_msg.getText().toString(), null, null);

        if (Build.VERSION.SDK_INT > 20) {
            Snackbar.make(view, "Message Successfully Sent !", Snackbar.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), "Message Successfully Sent !", Toast.LENGTH_SHORT).show();
        }


    }


    public void setInfo(final String offer_id, final View view) {

        String url = Config.ride_info;
        //    pd.show();
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("result");
                    JSONObject obj = result.getJSONObject(0);
                    String from_location = obj.getString("from_location");
                    String to_location = obj.getString("to_location");
                    String depart_date = obj.getString("depart_date");
                    String depart_time = obj.getString("depart_time");
                    String avail_seat = obj.getString("avail_seat");
                    String price = obj.getString("price");
                    String comment = obj.getString("comment");
                    email = obj.getString("email");
                    mobile = obj.getString("mobile");
                    dregId = obj.getString("reg_id");
                    userinfo_offer_id = obj.getString("offer_id");
                    uname = obj.getString("name");

                  //  facility,car_name,type,music,drink,smoke,food,taxi_service,driver_name,agency_name
                    String facility_one = obj.getString("facility");
                    String car_name_one = obj.getString("car_name");
                    String type_one = obj.getString("type");
                    String music_one = obj.getString("music");
                    String drink_one = obj.getString("drink");
                    String smoke_one = obj.getString("smoke");
                    String food_one = obj.getString("food");
                    String taxi_service_one=obj.getString("taxi_service");
                    String driver_name_one=obj.getString("driver_name");
                    String driver_work_at_one=obj.getString("agency_name");

                    //  facility,type,car_name,taxi_service,driver_name,driver_work_at;

                    int dist = Integer.parseInt(price) / 2;
                    float time = (float) dist / 50;
                    String t = time + "";




                    String arrTime[] = depart_time.split(":");
                    int t1 = Integer.parseInt(arrTime[0]);
                    int t2 = Integer.parseInt(arrTime[1]);
                    int t3 = Integer.parseInt(arrTime[2]);

                    String destTime[] = String.valueOf(t).split("\\.");
                    int nt1 = Integer.parseInt(destTime[0]);
                    int nt2 = Integer.parseInt(destTime[1]);

                    String hr = (t1 + nt1) + "";
                    String min = (t2 + nt2) + "";
                    rideDate.setText(depart_date);
                    rideTime.setText(depart_time);
                    rideSrcTime.setText(depart_time + "  ||  ");
                    rideDestTime.setText(hr + ":" + min + ":" + t3 + "  ||  ");
                    rideDest.setText(to_location);
                    rideSrc.setText(from_location);
                    rideAvailSeats.setText("Number Of Seats Available : " + avail_seat);
                    ridePrice.setText("Rs." + price + "/seat");
                    rideComment.setText(comment);
                    rideDistance.setText(dist + " Km");
                    tv_cont_no.setText(mobile);
                    tv_email.setText(email);
                    reg_id1.setText(dregId);
                    info_offer_id.setText(userinfo_offer_id);
                    tv_name.setText(uname);

                    //   //  facility,type,car_name,taxi_service,driver_name,driver_work_at;

                    facility.setText("Facility : "+facility_one);
                    type.setText("Type : "+type_one);
                    car_name.setText("Car Name : "+car_name_one);

                    if(taxi_service_one.equals("Yes")){
                        taxi_service.setText("Taxi Services : "+taxi_service_one);
                        driver_name.setVisibility(View.VISIBLE);
                        driver_work_at.setVisibility(View.VISIBLE);
                        driver_name.setText("Driver Name : "+driver_name_one);
                        driver_work_at.setText("Agency Name : "+driver_work_at_one);
                    } else if(taxi_service_one.equals("No")) {
                        taxi_service.setText("Taxi Services : " + taxi_service_one);
                        driver_name.setVisibility(View.GONE);
                        driver_work_at.setVisibility(View.GONE);

                    }



                } catch (Exception e) {
                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(view, "Check your Internet Connection !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Check your Internet Connection !", Toast.LENGTH_LONG).show();
                    }

                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (Build.VERSION.SDK_INT > 20) {
                            Snackbar.make(view, "Check your Internet Connection !", Snackbar.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Check your Internet Connection !", Toast.LENGTH_LONG).show();
                        }


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("offer_id", offer_id);
                // params.put("reg_id",regId);


                return params;
            }
        };

        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);
    }


    public void setNotification(final String rId, final String user_offerID, final String un, final String m, final String e) {
        String url = Config.gcmp;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {

                    Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("reg_id", rId);
                params.put("offer_id", user_offerID);
                params.put("user_name", un);
                params.put("mobile_no", m);
                params.put("email_id", e);


                return params;
            }
        };

        MyApplication.getInstance().addToReqQueue(postRequest);
    }

}



