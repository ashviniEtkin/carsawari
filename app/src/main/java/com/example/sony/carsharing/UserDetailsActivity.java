package com.example.sony.carsharing;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserDetailsActivity extends AppCompatActivity {

    ListView lv1;

    List<UserInfo> list;
    CustomAdaptor adaptor;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        lv1=(ListView)findViewById(R.id.listview);
      //  adaptor=new CustomAdaptor();



        list= new ArrayList<UserInfo>();

        pd = new ProgressDialog(this);
        pd.setTitle("Etkin");
        pd.setMessage("Loading........");
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        showdata();



    }

    public void showdata() {
        String url = Config.urldisplay;
        pd.show();

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        pd.dismiss();
                        try {


                            // JSONObject jsonObject=new JSONObject(response);

                            JSONArray result = response.getJSONArray("result");


                            for (int i = 0; i < result.length(); i++) {
                                JSONObject obj = result.getJSONObject(i);
                                int mobile = obj.getInt("mobile");
                                String name = obj.getString("name");
                                String email = obj.getString("email");
                                String password = obj.getString("password");
                                String address = obj.getString("address");
                                String city = obj.getString("city");
                                String gender = obj.getString("gender");
                                UserInfo objuser = new UserInfo(name, email, password, gender, mobile, address, city);
                                list.add(objuser);

                            }
                            CustomAdaptor adaptor = new CustomAdaptor(UserDetailsActivity.this, list);
                            lv1.setAdapter(adaptor);





                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(),
                                    "error loading data",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                if (error.networkResponse ==null) {
                    if (error.getClass().equals(TimeoutError.class)) {

                        Toast.makeText(getApplicationContext(),
                                "Oops. Timeout error!",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);

    }




}

