package com.example.sony.carsharing;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FindList extends Fragment {

    List<FindPojo> arr;
    ListView l;
    FindListAdapter findListAdapter;
    FindPojo objuser;
    public static String eId, offer_id;
    FragmentManager manager;
    FragmentTransaction transaction;
    SharedPreferences pref;


    public FindList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_find_list, container, false);
        arr = new ArrayList<FindPojo>();

        l = (ListView) v.findViewById(R.id.listview_of_find_ride_later);
        //                          findListAdapter = new FindListAdapter(getActivity(), arr);

        pref = getActivity().getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        eId = pref.getString("EMAIL", LoginActivity.email1);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                RideInfo rideInfo = new RideInfo();
                TextView tv = (TextView) view.findViewById(R.id.offer_id);
                offer_id = tv.getText().toString();
                rideInfo.setInfo(offer_id, view);
                for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }


                manager = getActivity().getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide2, R.anim.slide1);
                transaction.add(R.id.Homecontainer, rideInfo, "Ride Info");
                transaction.addToBackStack("Ride Info");
                transaction.commit();

              /*  rideInfo.setData()*/
            }
        });
//        l.setAdapter(findListAdapter);

        showList();
        return v;
    }


    public void showList() {
        String url = Config.urlFindRide;
        //    pd.show();

        //  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("result");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject obj = result.getJSONObject(i);
                        String from_location = obj.getString("from_location");
                        String offer_id = obj.getString("offer_id");
                        String email = obj.getString("email");
                        String to_location = obj.getString("to_location");
                        String depart_date = obj.getString("depart_date");
                        String depart_time = obj.getString("depart_time");
                        String avail_seat = obj.getString("avail_seat");
                        String price = obj.getString("price");
                        String facility = obj.getString("facility");
                        String car_name = obj.getString("car_name");
                        String type = obj.getString("type");
                        String music = obj.getString("music");
                        String drink = obj.getString("drink");
                        String smoke = obj.getString("smoke");
                        String food = obj.getString("food");
                        String name = obj.getString("name");
                        String age = obj.getString("age");
                        objuser = new FindPojo(offer_id, email, from_location, to_location, depart_date, depart_time, avail_seat, price, facility,car_name,type,music,drink,smoke,food,name, age);
                        arr.add(objuser);
                    }
                    findListAdapter = new FindListAdapter(getActivity(), arr);
                    l.setAdapter(findListAdapter);

                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("from_location", HomeFragment.src);
                params.put("to_location", HomeFragment.dest);
                params.put("email", eId);

                return params;
            }
        };

        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //  requestQueue.add(postRequest);
        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);
    }

}
