package com.example.sony.carsharing;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class OfferRideFragment extends Fragment {

    TextView fromLoc, toLoc, tv_depart_date, tv_depart_time, tv_return_date, tv_return_time, return_date_n_time, Price;
    EditText ride_comment;
    ImageButton incPrice, decPrice;
    Button btnOffer;
    CheckBox cb_round_trip, cb_terms_cond;
    boolean c;
    String toggle_check;
    public static int final_price;

    LinearLayout return_date_time_layout;
    Calendar cl;
    int yr, month, date1, hr, min;
    public static String from_loc, to_loc, depart_date, depart_time, price, avail_seats, comment, return_date, return_time;
    View v;
    Spinner spSeat, sp_facility;
    double prc, dist;
    public static ArrayList<RidePojo> arr = new ArrayList<RidePojo>();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static String emailId;

    CheckBox music, smoking, food, drink;

    double srcLat, srcLng, destLat, destLng;
    EditText car_name, driver_work_at, driver_name;
    Spinner sp_type;
    ToggleButton toggleButton1;


    public OfferRideFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_offer_ride, container, false);

        music = (CheckBox) v.findViewById(R.id.music);
        smoking = (CheckBox) v.findViewById(R.id.smoking);
        food = (CheckBox) v.findViewById(R.id.food);
        drink = (CheckBox) v.findViewById(R.id.drink);


        toggleButton1 = (ToggleButton) v.findViewById(R.id.toggleButton1);

        sp_type = (Spinner) v.findViewById(R.id.sp_type);
        sp_facility = (Spinner) v.findViewById(R.id.sp_facility);

        car_name = (EditText) v.findViewById(R.id.car_name);

        driver_work_at = (EditText) v.findViewById(R.id.driver_work_at);
        driver_name = (EditText) v.findViewById(R.id.driver_name);


        fromLoc = (TextView) v.findViewById(R.id.fromLoc);
        toLoc = (TextView) v.findViewById(R.id.toLoc);
        cb_round_trip = (CheckBox) v.findViewById(R.id.cb_round_trip);
        tv_depart_date = (TextView) v.findViewById(R.id.tv_depart_date);
        tv_depart_time = (TextView) v.findViewById(R.id.tv_time);
        return_date_time_layout = (LinearLayout) v.findViewById(R.id.return_date_time_layout);
        return_date_n_time = (TextView) v.findViewById(R.id.return_date_n_time);
        tv_return_date = (TextView) v.findViewById(R.id.tv_return_date);
        tv_return_time = (TextView) v.findViewById(R.id.tv_return_time);
        Price = (TextView) v.findViewById(R.id.price);
        spSeat = (Spinner) v.findViewById(R.id.spSeat);
        incPrice = (ImageButton) v.findViewById(R.id.incPrice);
        decPrice = (ImageButton) v.findViewById(R.id.decPrice);
        ride_comment = (EditText) v.findViewById(R.id.ride_comment);
        cb_terms_cond = (CheckBox) v.findViewById(R.id.cb_terms_cond);
        btnOffer = (Button) v.findViewById(R.id.btnOffer);

        cb_terms_cond.setText(Html.fromHtml("I have read and accept the <a href='#'> Terms and Condtions </a> and certify that I am over 18 years old,hold a driving licence and have car insurance."));

        srcLat = Double.parseDouble(HomeFragment.srclat);
        srcLng = Double.parseDouble(HomeFragment.srclng);
        destLat = Double.parseDouble(HomeFragment.destlat);
        destLng = Double.parseDouble(HomeFragment.destlng);

        cl = Calendar.getInstance();
        yr = cl.get(Calendar.YEAR);
        month = cl.get(Calendar.MONTH);
        date1 = cl.get(Calendar.DAY_OF_MONTH);
        hr = cl.get(Calendar.HOUR_OF_DAY);
        min = cl.get(Calendar.MINUTE);

        List<Integer> NoOfSeats = new ArrayList<Integer>();
        NoOfSeats.add(1);
        NoOfSeats.add(2);
        NoOfSeats.add(3);
        NoOfSeats.add(4);
        NoOfSeats.add(5);
        NoOfSeats.add(6);
        NoOfSeats.add(7);
        NoOfSeats.add(8);
        NoOfSeats.add(9);
        NoOfSeats.add(10);


        List<String> type = new ArrayList<String>();
        type.add("Private");
        type.add("Public");

        List<String> fac = new ArrayList<String>();
        fac.add("NON AC");
        fac.add("AC");


        ArrayAdapter<String> adapter_type = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, type);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_type.setAdapter(adapter_type);


        ArrayAdapter<String> adapter_fac = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, fac);
        adapter_fac.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_facility.setAdapter(adapter_fac);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this.getActivity(), android.R.layout.simple_spinner_item, NoOfSeats);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSeat.setAdapter(adapter);

        fromLoc.setText(HomeFragment.src);
        toLoc.setText(HomeFragment.dest);


        music.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    music.setText("1");
                } else {
                    music.setText("0");

                }
            }
        });

        drink.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    drink.setText("1");
                } else {
                    drink.setText("0");

                }
            }
        });

        food.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    food.setText("1");
                } else {
                    food.setText("0");

                }
            }
        });

        smoking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    smoking.setText("1");
                } else {
                    smoking.setText("0");

                }
            }
        });


        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggle_check = isChecked + "";
                c = isChecked;
                if (c == true) {
                    driver_name.setVisibility(View.VISIBLE);
                    driver_work_at.setVisibility(View.VISIBLE);
                } else {
                    driver_name.setVisibility(View.GONE);
                    driver_work_at.setVisibility(View.GONE);

                }


            }
        });

        dist = HomeFragment.calculateDistance(srcLat, srcLng, destLat, destLng);
        //  String distarr[]=dist.split()







        sp_facility.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String s = sp_facility.getSelectedItem().toString();

                if (s.equals("AC")) {
                    prc = dist * 5;
                    final_price = (int) prc;
                    price = final_price + "";
                    Price.setText(price);
                }
                if (s.equals("NON AC")) {
                    prc = dist * 2;
                    final_price = (int) prc;
                    price = final_price + "";
                    Price.setText(price);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        pref = getActivity().getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        emailId = pref.getString("EMAIL", LoginActivity.email1);
        String i = emailId;


        cb_round_trip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    return_date_time_layout.setVisibility(View.VISIBLE);
                    return_date_n_time.setVisibility(View.VISIBLE);
                } else {
                    return_date_time_layout.setVisibility(View.GONE);
                    return_date_n_time.setVisibility(View.GONE);
                }
            }
        });

        tv_depart_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDepartureDate();

            }
        });
        tv_depart_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDepartureTime();

            }
        });
        tv_return_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReturnDate();

            }
        });
        tv_return_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReturnTime();

            }
        });

        incPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* prc = prc + 10;
                int incp = (int) prc;
                price = incp + "";
                Price.setText(price);*/

                prc = prc + 10;
                int incp = (int) prc;
                price = incp + "";
                Price.setText(price);
            }
        });
        decPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prc = prc + 10;
                int incp = (int) prc;
                price = incp + "";
                Price.setText(price);
            }
        });


        cb_terms_cond.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btnOffer.setClickable(true);
                    btnOffer.setBackgroundColor(getResources().getColor(R.color.md_indigo_700));

                } else {
                    btnOffer.setClickable(false);
                    btnOffer.setBackgroundColor(getResources().getColor(R.color.md_blue_grey_500));
                }
            }
        });


        btnOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (smoking.isChecked()) {
                    smoking.setText("1");
                } else {
                    smoking.setText("0");

                }

                if (music.isChecked()) {
                    music.setText("1");
                } else {
                    music.setText("0");

                }

                if (drink.isChecked()) {
                    drink.setText("1");
                } else {
                    drink.setText("0");

                }

                if (food.isChecked()) {
                    smoking.setText("1");
                } else {
                    food.setText("0");

                }

                if (toggleButton1.isChecked()) {
                    toggle_check = "Yes";
                } else {
                    toggle_check = "No";
                }


                if (cb_terms_cond.isChecked()) {
                    if (cb_round_trip.isChecked()) {
                        if (tv_depart_date.getText().toString().equals("") || tv_depart_time.getText().toString().equals("") || tv_return_date.getText().toString().equals("") || tv_return_time.getText().toString().equals("")) {
                            if (Build.VERSION.SDK_INT > 20) {
                                Snackbar.make(v, "Select Date and Time !", Snackbar.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Select Date and Time !", Toast.LENGTH_LONG).show();
                            }


                        } else {
                            oneWayRide(v);
                            twoWayRide(v);
                            Intent in = new Intent(getActivity(), HomeActivity.class);
                            startActivity(in);

                        }
                    } else if (!cb_round_trip.isChecked()) {

                        if (tv_depart_date.getText().toString().equals("") || tv_depart_time.getText().toString().equals("")) {
                            if (Build.VERSION.SDK_INT > 20) {
                                Snackbar.make(v, "Select Date and Time !", Snackbar.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), "Select Date and Time !", Toast.LENGTH_LONG).show();
                            }


                        } else {
                            oneWayRide(v);
                            Intent in = new Intent(getActivity(), HomeActivity.class);
                            startActivity(in);

                        }
                    }
                } else {

                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Agree to Terms and Conditions !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Agree to Terms and Conditions !", Toast.LENGTH_LONG).show();
                    }


                }


            }
        });

        return v;
    }

   /* public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.ac:
                if (checked) {
                    prc = dist * 5;
                    final_price = (int) prc;
                    price = final_price + "";
                    Price.setText(price);
                }
                break;
            case R.id.non_ac:
                if (checked) {
                    prc = dist * 2;
                    final_price = (int) prc;
                    price = final_price + "";
                    Price.setText(price);
                }
                break;
        }
    }*/

    public void getDepartureDate() {
        DatePickerDialog dg = new DatePickerDialog(getActivity(), departure_date_listener, yr, month, date1);
        dg.show();
    }

    public void getDepartureTime() {

        TimePickerDialog td = new TimePickerDialog(getActivity(), departure_time_listener, hr, min, true);
        td.show();

    }

    public void getReturnDate() {

        DatePickerDialog dg = new DatePickerDialog(getActivity(), return_date_listener, yr, month, date1);
        dg.show();

    }

    public void getReturnTime() {

        TimePickerDialog td = new TimePickerDialog(getActivity(), return_time_listener, hr, min, true);
        td.show();

    }

    DatePickerDialog.OnDateSetListener departure_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (tv_depart_date.getText().toString().equals(yr + "/" + month + "/" + date1)) {
                if (Build.VERSION.SDK_INT > 20) {
                    Snackbar.make(v, "Select Future date !", Snackbar.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Select Future date !", Toast.LENGTH_LONG).show();
                }


            } else {
                tv_depart_date.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
            }

        }
    };
    DatePickerDialog.OnDateSetListener return_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


            if (tv_return_date.getText().toString().equals(yr + "/" + month + "/" + date1)) {
                if (Build.VERSION.SDK_INT > 20) {
                    Snackbar.make(v, "Select Future date !", Snackbar.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Select Future Date !", Toast.LENGTH_LONG).show();
                }


            } else {
                tv_return_date.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
            }
        }
    };


    TimePickerDialog.OnTimeSetListener departure_time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            tv_depart_time.setText(hourOfDay + ":" + minute);

        }
    };
    TimePickerDialog.OnTimeSetListener return_time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            tv_return_time.setText(hourOfDay + ":" + minute);
        }
    };

    public void twoWayRide(final View view) {

        from_loc = fromLoc.getText().toString();
        to_loc = toLoc.getText().toString();
        depart_date = tv_depart_date.getText().toString();
        depart_time = tv_depart_time.getText().toString();
        return_date = tv_return_date.getText().toString();
        return_time = tv_return_time.getText().toString();
        price = Price.getText().toString();
        avail_seats = spSeat.getSelectedItem().toString();
        comment = ride_comment.getText().toString();
        to_loc = toLoc.getText().toString();

        // pd.show();
        String url = Config.urlPublish;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");
                    if (result == 1) {
                        if (Build.VERSION.SDK_INT > 20) {
                            Snackbar.make(view, "Congratulations!,Your ride has been published.", Snackbar.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Congratulations!,Your ride has been published." + result, Toast.LENGTH_SHORT).show();
                        }


                        //  startActivity(c);
                    } else {

                        if (Build.VERSION.SDK_INT > 20) {
                            Snackbar.make(view, "Failed to Published !", Snackbar.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Failed to Published !", Toast.LENGTH_LONG).show();
                        }


                    }
                } catch (Exception e) {


                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(view, "Check your Internet Connection !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Check your Internet Connection !", Toast.LENGTH_SHORT).show();
                    }


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  pd.dismiss();


                if (Build.VERSION.SDK_INT > 20) {
                    Snackbar.make(view, "Failed to published !Check Your Internet Connection.", Snackbar.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to published !Check Your Internet Connection." + error.toString(), Toast.LENGTH_SHORT).show();
                }


            }
        })

        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                //facility,car_name,type,music,drink,smoke,food,taxi_service,driver_name,agency_name

                params.put("email", emailId);
                params.put("from_location", to_loc);
                params.put("to_location", from_loc);
                params.put("depart_date", return_date);
                params.put("depart_time", return_time);
                params.put("price", price);
                params.put("avail_seat", avail_seats);
                params.put("comment", comment);
                params.put("facility", sp_facility.getSelectedItem().toString());
                params.put("car_name", car_name.getText().toString());
                params.put("type", sp_type.getSelectedItem().toString());
                params.put("music", music.getText().toString());
                params.put("drink", drink.getText().toString());
                params.put("smoke", smoking.getText().toString());
                params.put("food", food.getText().toString());
                params.put("taxi_service", toggle_check);
                params.put("driver_name", driver_name.getText().toString());
                params.put("agency_name", driver_work_at.getText().toString());
                return params;
            }
        };

        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);

    }


    public void oneWayRide(final View view) {

        from_loc = fromLoc.getText().toString();
        to_loc = toLoc.getText().toString();
        depart_date = tv_depart_date.getText().toString();
        depart_time = tv_depart_time.getText().toString();
        price = Price.getText().toString();
        avail_seats = spSeat.getSelectedItem().toString();
        comment = ride_comment.getText().toString();
        to_loc = toLoc.getText().toString();

        String url = Config.urlPublish;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");

                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(view, "Congratulations! Your ride has been published.", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Congratulations! Your ride has been published." + result, Toast.LENGTH_SHORT).show();
                    }


                    //    startActivity(c);
                } catch (Exception e) {

                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(view, "Check Your Internet Connection !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Check Your Internet Connection !", Toast.LENGTH_SHORT).show();
                    }


                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   pd.dismiss();

                if (Build.VERSION.SDK_INT > 20) {
                    Snackbar.make(view, "Failed to Publish ! Check Your Internet Connection.", Snackbar.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to Publish ! Check Your Internet Connection." + error.toString(), Toast.LENGTH_SHORT).show();
                }


            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();


                params.put("email", emailId);
                params.put("from_location", from_loc);
                params.put("to_location", to_loc);
                params.put("depart_date", depart_date);
                params.put("depart_time", depart_time);
                params.put("price", price);
                params.put("avail_seat", avail_seats);
                params.put("comment", comment);
                params.put("facility", sp_facility.getSelectedItem().toString());
                params.put("car_name", car_name.getText().toString());
                params.put("type", sp_type.getSelectedItem().toString());
                params.put("music", music.getText().toString());
                params.put("drink", drink.getText().toString());
                params.put("smoke", smoking.getText().toString());
                params.put("food", food.getText().toString());
                params.put("taxi_service", toggle_check);
                params.put("driver_name", driver_name.getText().toString());
                params.put("agency_name", driver_work_at.getText().toString());

                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);


    }

}
