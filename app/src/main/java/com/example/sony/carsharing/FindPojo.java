package com.example.sony.carsharing;

/**
 * Created by Lenovo on 2/15/2016.
 */
public class FindPojo {
    String depart_date, depart_time, src, dest, price, seat_avail, name, age, offer_id, email;

    String facility, car_name, type, music, drink, food, smoke;

    FindPojo(String offer_id, String email, String src, String dest, String date, String time, String seat_avails, String price,String facility,String car_name,String type,String music,String drink,String food,String smoke, String name, String age) {
        this.offer_id = offer_id;
        this.email = email;
        depart_date = date;
        depart_time = time;
        this.src = src;
        this.dest = dest;
        this.seat_avail = seat_avails;
        this.price = price;
        this.facility=facility;
        this.car_name=car_name;
        this.type=type;
        this.music=music;
        this.drink=drink;
        this.food=food;
        this.smoke=smoke;
        this.name = name;
        this.age = age;
    }


    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getSmoke() {
        return smoke;
    }

    public void setSmoke(String smoke) {
        this.smoke = smoke;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getDepart_date() {
        return depart_date;
    }

    public void setDepart_date(String depart_date) {
        this.depart_date = depart_date;
    }

    public String getDepart_time() {
        return depart_time;
    }

    public void setDepart_time(String depart_time) {
        this.depart_time = depart_time;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSeat_avail() {
        return seat_avail;
    }

    public void setSeat_avail(String seat_avail) {
        this.seat_avail = seat_avail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
