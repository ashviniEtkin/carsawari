package com.example.sony.carsharing;

public class Config {


    //for user registration
    public static String url = "http://192.168.1.4:8091/project_one/user1.php";

    public static String gcmp = "http://192.168.1.4:8091/project_one/gcmp.php";
    public static  String confirmBooking="http://192.168.1.4:8091/project_one/notify.php";
    public static String booking = "http://192.168.1.4:8091/project_one/book.php";

    public static String  forget_password= "http://192.168.1.4:8091/project_one/forget_password.php";
    
    public static String ride_info = "http://192.168.1.4:8091/project_one/ride_info.php";

    public static String twoWayRide = "http://192.168.1.4:8091/project_one/display_two_wayride_list.php";

    public static String oneWayRide = "http://192.168.1.4:8091/project_one/display_oneway_rideList.php";

    public static String editOfferRide = "http://192.168.1.4:8091/project_one/editOfferRide.php";

    public static String setImage = "http://192.168.1.4:8091/project_one/photos.php";


    //for displaying in list
    public static String urldisplay = "http://192.168.1.4:8091/project_one/display.php";

    public static String update_offer_ride = "http://192.168.1.4:8091/project_one/update_offer_list.php";


    public static String delete = "http://192.168.1.4:8091/project_one/delete.php";

    //for inserting data into databse for offer ride for 2 way journey


    public static String urlPublish = "http://192.168.1.4:8091/project_one/offerride_insertion.php";

    //for inserting data into database for offer ride for 1 way journey
    public static String urlOneWay = "http://192.168.1.4:8091/project_one/onewayjourney.php";

    //for login
    public static String urltv = "http://192.168.1.4:8091/project_one/comp_update.php";
    //public static String urlupdate="http://192.168.:8090/task1/updateRecord.php"; offerride_insertion,onewayjourney

    //for Find Ride List
    public static String urlFindRide = "http://192.168.1.4:8091/project_one/findride.php";

    public static String forHeaderName = "http://192.168.1.4:8091/project_one/for_login.php";
    //for image uploading
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://192.168.43.233/project_one/imageUpload.php";

    //folder to store uploaded images

    public static String headerImg = "http://192.168.1.4:8091/project_one/takeimage.php";

    public static final String IMAGE_DIRECTORY_NAME = "http://192.168.1.4:8091/project_one/uploads/";

}
