package com.example.sony.carsharing;


import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCity extends Fragment
{
    TextView tvCities;
    ListView lvCities;
    Context ctx;
    Resources resources;
    String[] Cities;
    CityListAdapter listAdapter;
    Communicator communicator;
    int position;
    FragmentManager manager;
    FragmentTransaction transaction;

    public void setData(int position,Context ctx)
    {
        this.ctx=ctx;
        this.position=position;
    }

    public FragmentCity()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        final View v = inflater.inflate(R.layout.fragment_city,container,false);
        tvCities = (TextView)v.findViewById(R.id.tvCities);
        lvCities=(ListView)v.findViewById(R.id.lvCities);


        changeData();


        lvCities.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // TextView tvCity = (TextView)v.findViewById(R.id.tvCity);
                String str = lvCities.getItemAtPosition(position).toString();
                communicator.setText(str);

                FragmentStates states = new FragmentStates();
              //  city.setData(position,getActivity());
                manager = getActivity().getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.contain,states, "states");
                transaction.addToBackStack("states");
                transaction.commit();

                RegistrationActivity.drawer.closeDrawer(GravityCompat.END);

             //   getActivity().finish();


            }
        });




        return v;
    }


    public void changeData()
    {
        resources = ctx.getResources();

        switch (position)
        {
            case 0: Cities = resources.getStringArray(R.array.AndamanNicobar);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 1: Cities = resources.getStringArray(R.array.AndhraPradesh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 2: Cities = resources.getStringArray(R.array.ArunachalPradesh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 3: Cities = resources.getStringArray(R.array.Assam);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 4: Cities = resources.getStringArray(R.array.Bihar);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;


            case 5: Cities = resources.getStringArray(R.array.Chhattisgarh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 6: Cities = resources.getStringArray(R.array.DadraAndNagarHaveli);
                lvCities.setAdapter(listAdapter);
                break;


            case 7: Cities = resources.getStringArray(R.array.DamanAndDiu);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 8: Cities = resources.getStringArray(R.array.Delhi);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 9: Cities = resources.getStringArray(R.array.Goa);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 10: Cities = resources.getStringArray(R.array.Gujrat);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 11: Cities = resources.getStringArray(R.array.Haryana);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 12: Cities = resources.getStringArray(R.array.HimachalPradesh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 13: Cities = resources.getStringArray(R.array.JammuKashmir);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 14: Cities = resources.getStringArray(R.array.Jharkhand);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 15: Cities = resources.getStringArray(R.array.Karnataka);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 16: Cities = resources.getStringArray(R.array.Kerala);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 17: Cities = resources.getStringArray(R.array.Lakshadweep);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 18: Cities = resources.getStringArray(R.array.MadhyaPradesh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 19: Cities = resources.getStringArray(R.array.Maharashtra);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 20: Cities = resources.getStringArray(R.array.Manipur);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 21: Cities = resources.getStringArray(R.array.Meghalaya);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 22: Cities = resources.getStringArray(R.array.Mizoram);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 23: Cities = resources.getStringArray(R.array.Nagaland);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 24: Cities = resources.getStringArray(R.array.Orissa);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 25: Cities = resources.getStringArray(R.array.Pondicherry);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 26: Cities = resources.getStringArray(R.array.Punjab);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 27: Cities = resources.getStringArray(R.array.Rajasthan);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 28: Cities = resources.getStringArray(R.array.Sikkim);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 29: Cities = resources.getStringArray(R.array.TamilNadu);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 30: Cities = resources.getStringArray(R.array.Tripura);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);


            case 31: Cities = resources.getStringArray(R.array.UttarPradesh);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 32: Cities = resources.getStringArray(R.array.Uttaranchal);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

            case 33: Cities = resources.getStringArray(R.array.WestBengal);
                listAdapter = new CityListAdapter(ctx, Cities);
                lvCities.setAdapter(listAdapter);
                break;

        }


    }


    public interface Communicator
    {
        public void setText(String city);
    }


    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        communicator = (Communicator)activity;
    }


}
