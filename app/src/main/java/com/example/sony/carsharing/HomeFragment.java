package com.example.sony.carsharing;


import android.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


    Button btn_find, btn_offer;
    ArrayList<LatLng> markerPoints;
    TextView tvDistanceDuration;
    Geocoder geocoder;
    List<Address> addresses;
    LatLng my_city;
    Double MyLat, MyLong;

    LocationManager locManager;
    LocationListener locListener;


    String address1, city, state, country, postalCode, knownName;
    String[] parts, str;
    String part1, part2;
    int zoom = 15;
    AutoCompleteTextView mAutocompleteTextView_to_loc;
    AutoCompleteTextView mAutocompleteTextView_from_loc;
    private GoogleMap mMap;
    private static final String LOG_TAG = "HomeFragment";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private TextView mNameTextView;
    private TextView mAddressTextView;
    private TextView mIdTextView;
    private TextView mPhoneTextView;
    private TextView mWebTextView;
    private TextView mAttTextView;
    private TextView mLatLngTextView;
    public String mlatlng;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    public static String src, dest, srclat, srclng, destlat, destlng;
    public static String address, placeid, curlocation;
    public static Double distance;
    private static final String TAG = "LocationAddress";


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_home, container, false);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).enableAutoManage(getActivity(), GOOGLE_API_CLIENT_ID, this)
                .addApi(Places.GEO_DATA_API).addConnectionCallbacks(this).build();

        mNameTextView = (TextView) v.findViewById(R.id.name);
        mAddressTextView = (TextView) v.findViewById(R.id.address);
        mIdTextView = (TextView) v.findViewById(R.id.place_id);
        mPhoneTextView = (TextView) v.findViewById(R.id.phone);
        mWebTextView = (TextView) v.findViewById(R.id.web);
        mAttTextView = (TextView) v.findViewById(R.id.att);
        mLatLngTextView = (TextView) v.findViewById(R.id.mLatLngtextView);
        tvDistanceDuration = (TextView) v.findViewById(R.id.tvDistanceDuration);

        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locListener = new MyLocationListener();


        mAutocompleteTextView_to_loc = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView_to_loc);
        mAutocompleteTextView_from_loc = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteTextView_from_loc);
        mAutocompleteTextView_to_loc.setThreshold(3);
        mAutocompleteTextView_from_loc.setThreshold(3);

        mAutocompleteTextView_from_loc.setOnItemClickListener(mAutocompleteClickListenerSource);
        mAutocompleteTextView_to_loc.setOnItemClickListener(mAutocompleteClickListenerDestination);


        mPlaceArrayAdapter = new PlaceArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);

        mAutocompleteTextView_to_loc.setAdapter(mPlaceArrayAdapter);
        mAutocompleteTextView_from_loc.setAdapter(mPlaceArrayAdapter);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        // ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // Initializing
        markerPoints = new ArrayList<LatLng>();
        geocoder = new Geocoder(getActivity(), Locale.getDefault());


           getMyCurrentLocation();


        btn_offer = (Button) v.findViewById(R.id.offer);
        btn_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mAutocompleteTextView_from_loc.getText().toString().equals("") || mAutocompleteTextView_to_loc.getText().toString().equals("")) {


                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Select Source And destination !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Select Source And destination !", Toast.LENGTH_LONG).show();
                    }

                } else if (mAutocompleteTextView_from_loc.getText().toString().equals(mAutocompleteTextView_to_loc.getText().toString())) {


                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Identical Source And Destination !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Identical Source And Destination !", Toast.LENGTH_LONG).show();
                    }


                } else {


/*
                    for(int i=0; i<getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++)
                    {
                        getActivity().getSupportFragmentManager().popBackStack();
                    }*/


                    OfferRideFragment offerRideFragment = new OfferRideFragment();
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
                    transaction.add(R.id.Homecontainer, offerRideFragment, "OfferRideFragment");
                    transaction.addToBackStack("OfferRideFragment");
                    transaction.commit();
                }


            }
        });


        btn_find = (Button) v.findViewById(R.id.find);


        btn_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mAutocompleteTextView_from_loc.getText().toString().equals("") || mAutocompleteTextView_to_loc.getText().toString().equals("")) {
                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Select Source And destination !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Select Source And destination !", Toast.LENGTH_LONG).show();
                    }


                } else if (mAutocompleteTextView_from_loc.getText().toString().equals(mAutocompleteTextView_to_loc.getText().toString())) {

                    if (Build.VERSION.SDK_INT > 20) {
                        Snackbar.make(v, "Identical Source And Destination !", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Identical Source And Destination !", Toast.LENGTH_LONG).show();
                    }

                } else {/*
                    for(int i=0; i<getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++)
                    {
                        getActivity().getSupportFragmentManager().popBackStack();
                    }*/

                    src = mAutocompleteTextView_from_loc.getText().toString();
                    dest = mAutocompleteTextView_to_loc.getText().toString();
                    FindList findList = new FindList();
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide2, R.anim.slide1);
                    transaction.add(R.id.Homecontainer, findList, "findRideFragment");
                    transaction.addToBackStack("findRideFragment");
                    transaction.commit();
                }


            }

        });


        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(getActivity());
        }
        super.onStop();
    }


    private void stopAutoManage() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.stopAutoManage(getActivity());
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerDestination = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            curlocation = String.valueOf(item.description);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackDestination);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            dest = curlocation;

            /*Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("DEST",dest);*/
        }

    };


    private AdapterView.OnItemClickListener mAutocompleteClickListenerSource = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            curlocation = String.valueOf(item.description);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackSource);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            src = curlocation;

           /* Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("SRC",src);*/

        }

    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackSource = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

            mNameTextView.setText(Html.fromHtml(place.getName() + ""));

            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            address = mAddressTextView.getText().toString();

            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            placeid = mIdTextView.getText().toString();


            mLatLngTextView.setText(Html.fromHtml(place.getLatLng() + ""));
            mlatlng = mLatLngTextView.getText().toString();

            parts = mlatlng.split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            String[] s3 = s1.split("\\(");
            srclat = s3[1];

            String[] s4 = s2.split("\\)");
            srclng = s4[0];

            LatLng India = new LatLng(Double.parseDouble(srclat), Double.parseDouble(srclng));
            Toast.makeText(getActivity(), srclat + "," + srclng, Toast.LENGTH_LONG).show();
            mMap.addMarker(new MarkerOptions().position(India).title("Marker in " + curlocation));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(India));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
            mMap.getMaxZoomLevel();

            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackDestination = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

            mNameTextView.setText(Html.fromHtml(place.getName() + ""));

            mAddressTextView.setText(Html.fromHtml(place.getAddress() + ""));
            address = mAddressTextView.getText().toString();

            mIdTextView.setText(Html.fromHtml(place.getId() + ""));
            placeid = mIdTextView.getText().toString();


            mLatLngTextView.setText(Html.fromHtml(place.getLatLng() + ""));
            mlatlng = mLatLngTextView.getText().toString();

            parts = mlatlng.split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            String[] s3 = s1.split("\\(");
            destlat = s3[1];

            String[] s4 = s2.split("\\)");
            destlng = s4[0];

            LatLng India = new LatLng(Double.parseDouble(destlat), Double.parseDouble(destlng));
            Toast.makeText(getActivity(), destlat + "," + destlng, Toast.LENGTH_LONG).show();
            mMap.addMarker(new MarkerOptions().position(India).title("Marker in " + curlocation));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(India));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
            mMap.getMaxZoomLevel();

            mPhoneTextView.setText(Html.fromHtml(place.getPhoneNumber() + ""));
            mWebTextView.setText(place.getWebsiteUri() + "");
            if (attributions != null) {
                mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }
        }
    };

    public static Double calculateDistance(Double srclat, Double srclng, Double destlat, Double destlng) {
        Double earthRadius = 6371.00;  //km
        Double lat = Math.toRadians(destlat - srclat);
        Double lng = Math.toRadians(destlng - srclng);

        Double d = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(Math.toRadians(srclat)) * Math.cos(Math.toRadians(destlat)) *
                Math.sin(lng / 2) * Math.sin(lng / 2);

        Double c = 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d));
        distance = (Double) (earthRadius * c);

        return distance;
    }


    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getActivity(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        my_city = new LatLng(location.getLatitude(), location.getLongitude());
        Toast.makeText(getActivity(), MyLat + "," + MyLong, Toast.LENGTH_LONG).show();
        mMap.addMarker(new MarkerOptions().position(my_city).title("Marker in India"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(my_city));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        mMap.getMaxZoomLevel();

        srclat = location.getLatitude() + "";
        srclng = location.getLongitude() + "";


        //  setLocation();


        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {

                // Already two locations
                if (markerPoints.size() > 1) {
                    markerPoints.clear();
                    mMap.clear();
                }

                // Adding new item to the ArrayList
                markerPoints.add(point);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(point);



                if (markerPoints.size() == 1) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                } else if (markerPoints.size() == 2) {
                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                }


                mMap.addMarker(options);


                if (markerPoints.size() >= 2) {
                    LatLng origin = markerPoints.get(0);
                    LatLng dest = markerPoints.get(1);


                    String url = getDirectionsUrl(origin, dest);

                    DownloadTask downloadTask = new DownloadTask();

                    downloadTask.execute(url);
                }
            }
        });
    }


    /*private void setLocation() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Location");
                query.whereContains("name", "rajesh");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        ParseObject obj = objects.get(0);
                        mMap.clear();
                        //mMap.setMyLocationEnabled(true);
                        // Add a marker in Sydney and move the camera
                        LatLng sydney = new LatLng(obj.getDouble("lat"), obj.getDouble("lang"));

                        mMap.addMarker(new MarkerOptions().position(sydney).title("Rajeshwar").snippet("Developer At Etkin Infotech Pvt Ltd"));
                        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoom));
                        setLocation();
                    }
                });


            }
        }.execute(null, null, null);
    }*/

    /*  *//* public class LocationAddress {
        private static final String TAG = "LocationAddress";*//*

        public static void getAddressFromLocation(final double latitude, final double longitude,
                                                  final Context context, final Handler handler) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    String result = null;
                    try {
                        List<Address> addressList = geocoder.getFromLocation(
                                latitude, longitude, 1);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                sb.append(address.getAddressLine(i)).append("\n");
                            }
                            sb.append(address.getLocality()).append("\n");
                            sb.append(address.getPostalCode()).append("\n");
                            sb.append(address.getCountryName());
                            result = sb.toString();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Unable connect to Geocoder", e);
                    } finally {
                        Message message = Message.obtain();
                        message.setTarget(handler);
                        if (result != null) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n\nAddress:\n" + result;
                            bundle.putString("address", result);
                            message.setData(bundle);
                        } else {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n Unable to get address for this lat-long.";
                            bundle.putString("address", result);
                            message.setData(bundle);
                        }
                        message.sendToTarget();
                    }
                }
            };
            thread.start();
        }

*/
    public void plus(View v) {
        if (zoom == 20) {

        } else {
            zoom++;
        }
    }

    public void minus(View v) {
        if (zoom == 1) {

        } else {
            zoom--;
        }
    }


    public class Broadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {


        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excp downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getActivity().getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);
            }

            tvDistanceDuration.setText("Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }


    }

    /*public void turnGPSOn() {
        try {

            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", true);
            getActivity().sendBroadcast(intent);

            String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!provider.contains("gps")) { //if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                getActivity().sendBroadcast(poke);
            }
        } catch (Exception e) {

        }
    }*/

    // Method to turn off the GPS
    public void turnGPSOff() {
        String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }
    }

    // turning off the GPS if its in on state. to avoid the battery+ drain.
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        turnGPSOff();
    }

    /**
     * Check the type of GPS Provider available at that instance and
     * collect the location informations
     *
     * @Output Latitude and Longitude
     */
    public void getMyCurrentLocation() {


        try {
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        //don't start listeners if no provider is enabled
        //if(!gps_enabled && !network_enabled)
        //return false;

        if (gps_enabled) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);

        }


        if (gps_enabled) {
            location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


        }


        if (network_enabled && location == null) {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);

        }


        if (network_enabled && location == null) {
            location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        }

        if (location != null) {

            MyLat = location.getLatitude();
            MyLong = location.getLongitude();


        } else {
            Location loc = getLastKnownLocation(getActivity());
            if (loc != null) {

                MyLat = loc.getLatitude();
                MyLong = loc.getLongitude();


            }
        }

        //  locManager.removeUpdates(locListener); // removes the periodic updates from location listener to //avoid battery drainage. If you want to get location at the periodic intervals call this method using //pending intent.

        try {
// Getting address from found locations.

            addresses = geocoder.getFromLocation(MyLat, MyLong, 1);
            address1 = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();
            // you can get more details other than this . like country code, state code, etc.


            System.out.println(" StateName " + StateName);
            System.out.println(" CityName " + CityName);
            System.out.println(" CountryName " + CountryName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mAutocompleteTextView_from_loc.setText(address1 + "," + city + "," + state + "," + country);
        src = mAutocompleteTextView_from_loc.getText().toString();

    }

    // Location listener class. to get location.
    public class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            if (location != null) {
            }
        }

        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    }

    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    Location location;


    String CityName = "";
    String StateName = "";
    String CountryName = "";

// below method to get the last remembered location. because we don't get locations all the times .At some instances we are unable to get the location from GPS. so at that moment it will show us the last stored location.

    public static Location getLastKnownLocation(Context context) {
        Location location = null;
        LocationManager locationmanager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List list = locationmanager.getAllProviders();
        boolean i = false;
        Iterator iterator = list.iterator();
        do {
            //System.out.println("---------------------------------------------------------------------");
            if (!iterator.hasNext())
                break;
            String s = (String) iterator.next();
            //if(i != 0 && !locationmanager.isProviderEnabled(s))
            if (i != false && !locationmanager.isProviderEnabled(s))
                continue;
            // System.out.println("provider ===> "+s);

            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //  return TODO;
            }
            Location location1 = locationmanager.getLastKnownLocation(s);
            if (location1 == null)
                continue;
            if (location != null) {
                //System.out.println("location ===> "+location);
                //System.out.println("location1 ===> "+location);
                float f = location.getAccuracy();
                float f1 = location1.getAccuracy();
                if (f >= f1) {
                    long l = location1.getTime();
                    long l1 = location.getTime();
                    if (l - l1 <= 600000L)
                        continue;
                }
            }
            location = location1;
            // System.out.println("location  out ===> "+location);
            //System.out.println("location1 out===> "+location);
            i = locationmanager.isProviderEnabled(s);
            // System.out.println("---------------------------------------------------------------------");
        } while (true);
        return location;
    }
}
