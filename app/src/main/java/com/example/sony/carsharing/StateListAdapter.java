package com.example.sony.carsharing;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Lenovo on 1/29/2016.
 */
public class StateListAdapter extends BaseAdapter
{
    Context ctx;
    String[] states;


    public StateListAdapter(Context ctx,String[] states)
    {
        this.ctx = ctx;
        this.states = states;
    }

    @Override
    public int getCount() {
        return states.length;
    }

    @Override
    public Object getItem(int position) {
        return states[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        ViewHolder holder = null;

        if (v == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.state_list,null);
            holder.tvStateName= (TextView)v.findViewById(R.id.tvStateName);
            v.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) v.getTag();
        }

        holder.tvStateName.setText(states[position]);

        return v;
    }


    public class ViewHolder
    {
        TextView tvStateName;
    }

}
