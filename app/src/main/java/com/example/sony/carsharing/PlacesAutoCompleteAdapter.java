package com.example.sony.carsharing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Lenovo on 2/9/2016.
 */
public class PlacesAutoCompleteAdapter  extends ArrayAdapter<String> implements Filterable
{

    ArrayList<String> resultList;
    ArrayList<String> placeIdList;
   // public static String placeId;
   // String[] LatLng= new String[2];


    Context mContext;
    int mResource;

    PlaceAPI mPlaceAPI = new PlaceAPI();

    public PlacesAutoCompleteAdapter(Context context, int resource)
    {
        super(context, resource);
        mContext = context;
        mResource = resource;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position)
    {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null)
                {
                    resultList = mPlaceAPI.autocomplete(constraint.toString());
                   // placeIdList = mPlaceAPI.getPlaceId(constraint.toString());
                   // LatLng = mPlaceAPI.getLatLng();

                    resultList.add("footer");
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }




            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else
                {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view;

        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (position != (resultList.size() - 1))
            view = inflater.inflate(R.layout.autocomplete_list_item, null);
        else
            view = convertView;
        //}
        //else {
        //    view = convertView;
        //}

        if (position != (resultList.size() - 1)) {
            TextView autocompleteTextView = (TextView) view.findViewById(R.id.autocompleteText);
            autocompleteTextView.setText(resultList.get(position));
          //  placeId = placeIdList.get(position);
            //LatLng = mPlaceAPI.getLatLng();
        }


        return view;
    }


}

class ViewHolder
{
    TextView autocompleteTextView;

}
