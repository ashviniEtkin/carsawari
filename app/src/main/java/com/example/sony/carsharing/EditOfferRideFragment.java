package com.example.sony.carsharing;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditOfferRideFragment extends Fragment {

    public static Double distance;
    public static String from_src, to_dest, dest_date, dest_time;

    TextView fromLoc, toLoc, tv_depart_date, tv_depart_time, tv_return_date, tv_return_time, return_date_n_time, Price;
    EditText ride_comment;
    ImageButton incPrice, decPrice;
    Button btnOffer;
    CheckBox cb_round_trip, cb_terms_cond;
    LinearLayout return_date_time_layout;
    Calendar cl;
    int yr, month, date1, hr, min;
    public static String from_loc, to_loc, depart_date, depart_time, price, avail_seats, comment, return_date, return_time,oId;
  //  View v;
    Spinner spSeat;
    int prc;
    View v;
    public static ArrayList<RidePojo> arr = new ArrayList<RidePojo>();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static String emailId;



    Context context;

    public EditOfferRideFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

         v = inflater.inflate(R.layout.fragment_edit_offer_ride, container, false);
        fromLoc = (TextView) v.findViewById(R.id.edit_fromLoc);
        toLoc = (TextView) v.findViewById(R.id.edit_toLoc);
        cb_round_trip = (CheckBox) v.findViewById(R.id.edit_cb_round_trip);
        tv_depart_date = (TextView) v.findViewById(R.id.edit_tv_depart_date);
        tv_depart_time = (TextView) v.findViewById(R.id.edit_tv_time);
        return_date_time_layout = (LinearLayout) v.findViewById(R.id.edit_return_date_time_layout);
        return_date_n_time = (TextView) v.findViewById(R.id.edit_return_date_n_time);
        tv_return_date = (TextView) v.findViewById(R.id.edit_tv_return_date);
        tv_return_time = (TextView) v.findViewById(R.id.edit_tv_return_time);
        Price = (TextView) v.findViewById(R.id.edit_price);
        spSeat = (Spinner) v.findViewById(R.id.edit_spSeat);
        incPrice = (ImageButton) v.findViewById(R.id.edit_incPrice);
        decPrice = (ImageButton) v.findViewById(R.id.edit_decPrice);
        ride_comment = (EditText) v.findViewById(R.id.edit_ride_comment);
        cb_terms_cond = (CheckBox) v.findViewById(R.id.edit_cb_terms_cond);
        btnOffer = (Button) v.findViewById(R.id.edit_btnOffer);

        cb_terms_cond.setText(Html.fromHtml("I have read and accept the <a href='#'> Terms and Condtions </a> and certify that I am over 18 years old,hold a driving licence and have car insurance."));

        cl = Calendar.getInstance();
        yr = cl.get(Calendar.YEAR);
        month = cl.get(Calendar.MONTH);
        date1 = cl.get(Calendar.DAY_OF_MONTH);
        hr = cl.get(Calendar.HOUR_OF_DAY);
        min = cl.get(Calendar.MINUTE);

        fromLoc.setText(OfferListAdapter.src);
        toLoc.setText(OfferListAdapter.dest);
        tv_depart_date.setText(OfferListAdapter.date);
        tv_depart_time.setText(OfferListAdapter.time);
        Price.setText(OfferListAdapter.prc);
        ride_comment.setText(OfferListAdapter.c);
        oId=OfferListAdapter.offer_id;


        prc = Integer.parseInt(Price.getText().toString());

        List<Integer> NoOfSeats = new ArrayList<Integer>();
        NoOfSeats.add(1);
        NoOfSeats.add(2);
        NoOfSeats.add(3);
        NoOfSeats.add(4);
        NoOfSeats.add(5);
        NoOfSeats.add(6);
        NoOfSeats.add(7);
        NoOfSeats.add(8);
        NoOfSeats.add(9);
        NoOfSeats.add(10);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this.getActivity(), android.R.layout.simple_spinner_item, NoOfSeats);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSeat.setAdapter(adapter);

        pref = getActivity().getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        emailId = pref.getString("EMAIL", LoginActivity.email1);

        cb_round_trip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    return_date_time_layout.setVisibility(View.VISIBLE);
                    return_date_n_time.setVisibility(View.VISIBLE);

                } else {


                    return_date_time_layout.setVisibility(View.GONE);
                    return_date_n_time.setVisibility(View.GONE);
                }
            }
        });

        tv_depart_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDepartureDate();

            }
        });
        tv_depart_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDepartureTime();

            }
        });
        tv_return_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReturnDate();

            }
        });
        tv_return_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getReturnTime();

            }
        });

        incPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prc = prc + 10;
                price = prc+"";
                Price.setText(price);
            }
        });
        decPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prc = prc - 10;
                price = prc + "";
                Price.setText(price);
            }
        });


        cb_terms_cond.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btnOffer.setClickable(true);
                    btnOffer.setBackgroundColor(getResources().getColor(R.color.md_indigo_700));

                } else {
                    btnOffer.setClickable(false);
                    btnOffer.setBackgroundColor(getResources().getColor(R.color.md_blue_grey_500));
                }
            }
        });

        btnOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                from_loc = fromLoc.getText().toString();
                to_loc = toLoc.getText().toString();
                depart_date = tv_depart_date.getText().toString();
                depart_time = tv_depart_time.getText().toString();
                return_date = tv_return_date.getText().toString();
                return_time = tv_return_time.getText().toString();
                price = Price.getText().toString();
                avail_seats = spSeat.getSelectedItem().toString();
                comment = ride_comment.getText().toString();
                to_loc = toLoc.getText().toString();


                if (cb_terms_cond.isChecked())
                {
                    if (cb_round_trip.isChecked())
                    {
                        if (tv_depart_date.getText().toString().equals("") || tv_depart_time.getText().toString().equals("") || tv_return_date.getText().toString().equals("") || tv_return_time.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Invalid Date/Time!", Toast.LENGTH_LONG).show();
                        } else {


                            newOffer();

                        }
                    } else if (!cb_round_trip.isChecked()) {

                        if (tv_depart_date.getText().toString().equals("") || tv_depart_time.getText().toString().equals("")) {
                            Toast.makeText(getActivity(), "Invalid Date/Time!", Toast.LENGTH_LONG).show();
                        } else {

                            update();
                        }
                    }

                    Intent in = new Intent(getActivity(), HomeActivity.class);
                    startActivity(in);

                }
                else if (!cb_terms_cond.isChecked())
                {

                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(v, "Agree to Terms and Conditions !", Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(),"Agree to Terms and Conditions !",Toast.LENGTH_LONG).show();
                    }


                }
            }

        });



        return v;
    }



    public void getDepartureDate() {
        DatePickerDialog dg = new DatePickerDialog(getActivity(), departure_date_listener, yr, month, date1);
        dg.show();
    }

    public void getDepartureTime() {

        TimePickerDialog td = new TimePickerDialog(getActivity(), departure_time_listener, hr, min, true);
        td.show();

    }

    public void getReturnDate() {

        DatePickerDialog dg = new DatePickerDialog(getActivity(), return_date_listener, yr, month, date1);
        dg.show();

    }

    public void getReturnTime() {

        TimePickerDialog td = new TimePickerDialog(getActivity(), return_time_listener, hr, min, true);
        td.show();

    }

    DatePickerDialog.OnDateSetListener departure_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            tv_depart_date.setText(year + "/" +( monthOfYear+1) + "/" + dayOfMonth);


        }
    };
    DatePickerDialog.OnDateSetListener return_date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            tv_return_date.setText(year + "/" + (monthOfYear+1) + "/" + dayOfMonth);

        }
    };


    TimePickerDialog.OnTimeSetListener departure_time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            tv_depart_time.setText(hourOfDay + ":" + minute);

        }
    };
    TimePickerDialog.OnTimeSetListener return_time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            tv_return_time.setText(hourOfDay + ":" + minute);
        }
    };

    public void newOffer() {
        // pd.show();
        String url = Config.urlPublish;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");
                    if(result==1) {
                        Toast.makeText(getActivity(), "Congratulations!, Your ride has been published." + result, Toast.LENGTH_SHORT).show();
                        update();

                    } else
                    {
                        Toast.makeText(getActivity(),"Failed to published, Check Your Internet Connection !",Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(),
                            "Please Check Your Internet Connection!",
                            Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  pd.dismiss();
                Toast.makeText(getActivity(),
                        "Failed to published, Please Check Your Internet Connection!" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", emailId);
                params.put("from_location", to_loc);
                params.put("to_location", from_loc);
                params.put("depart_date", return_date);
                params.put("depart_time", return_time);
                params.put("price", price);
                params.put("avail_seat", avail_seats);
                params.put("comment", comment);
                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);

    }


    public void update() {

        String url = Config.update_offer_ride;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");
                    if(result==1) {
                        Toast.makeText(getActivity(), "Successfully Update" + result, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getActivity(),"Ride not published please Check your Internet Connection",Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(),
                            "Please Check Your Internet Connection!"+e.toString(),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //   pd.dismiss();
                Toast.makeText(getActivity(),
                        "Failed to Publish" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", emailId);
                params.put("from_location", from_loc);
                params.put("to_location", to_loc);
                params.put("depart_date", depart_date);
                params.put("depart_time", depart_time);
                params.put("price", price);
                params.put("avail_seat", avail_seats);
                params.put("comment", comment);
                params.put("offer_id",oId);
                return params;
            }
        };

        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(postRequest);


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);


    }
}

