package com.example.sony.carsharing;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.jar.*;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class RegistrationActivity extends AppCompatActivity implements FragmentCity.Communicator {
    public static String encoded_string, image_name;
    Bitmap bm;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Uri file_uri;
    private File file;
    String PROJECT_NUMBER = "768165108362";
    Button btnRegister;
    ProgressDialog pd;
    EditText etName, etEmail, etPassword, etConfPassword, etMobile, etAddress, etAge;
    TextView tvCity;
    RadioGroup rgGender;
    RadioButton rbgender, rbMale, rbFemale;
    Intent c;
    String emailPattern;
    String name, password, email, address, city, mobile, age;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final String TAG = MainActivity.class.getSimpleName();
    //ArrayList<UserInfo> arr=new ArrayList<UserInfo>();
    public static DrawerLayout drawer;
    NavigationView navigationView;
    private int PICK_IMAGE_REQUEST = 1;
    private Uri fileUri; // file url to store image/video
    private ImageButton btnCapturePicture;
    TextView reg_id;
   // CheckBox terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfPassword = (EditText) findViewById(R.id.etConfPassword);
        etAddress = (EditText) findViewById(R.id.etAddress);
        tvCity = (TextView) findViewById(R.id.tvCity);
        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        etAge = (EditText) findViewById(R.id.etAge);
        etMobile = (EditText) findViewById(R.id.etMobile);
        btnCapturePicture = (ImageButton) findViewById(R.id.btn_upload_photo);
      //  terms = (CheckBox)findViewById(R.id.terms);
        //terms.setText(Html.fromHtml("I Agree to  the<a href='#'> Terms and Condtions </a>"));
        reg_id = (TextView) findViewById(R.id.reg_id);

      /*  terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    btnRegister.setClickable(true);
                    btnRegister.setBackgroundColor(getResources().getColor(R.color.md_indigo_700));
                }
                else
                {
                    btnRegister.setClickable(false);
                    btnRegister.setBackgroundColor(getResources().getColor(R.color.md_blue_grey_500));
                }
            }

        });*/



        btnCapturePicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                setImage();
            }

        });


        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);

        tvCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                drawer.openDrawer(GravityCompat.END);


            }
        });

     /*   GCMClientManager pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                reg_id.setText(registrationId);
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });*/


        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        name = etName.getText().toString();
        password = etPassword.getText().toString();
        email = etEmail.getText().toString();
        address = etAddress.getText().toString();
        mobile = etMobile.getText().toString();
        city = tvCity.getText().toString();
        age = etAge.getText().toString();
        // gender = rbgender.getText().toString();
        c = new Intent(RegistrationActivity.this, LoginActivity.class);


        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int selectedId = rgGender.getCheckedRadioButtonId();
                rbgender = (RadioButton) findViewById(selectedId);

               /* if (terms.isChecked()) {*/

                    if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                        etName.setError("Enter name");
                        return;
                    } else if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                        etEmail.setError("Enter email");
                        return;
                    } else if (TextUtils.isEmpty(etPassword.getText().toString().trim())||(etPassword.getText().toString().trim().length()<6)) {
                        etPassword.setError("Enter password And Password must have minimum 6 characters");
                        return;
                    } else if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
                        etAddress.setError("Enter Address");
                        return;
                    } else if (TextUtils.isEmpty(etConfPassword.getText().toString().trim())) {
                        etConfPassword.setError("Enter password");
                        return;
                    } else if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
                        etMobile.setError("Enter Mobile Number");
                    } else if (TextUtils.isEmpty(etAge.getText().toString().trim())) {
                        etAge.setError("Enter Age");
                    } else if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
                        etAddress.setError("Enter Mobile Number");
                    } else if (!etEmail.getText().toString().matches(emailPattern)) {
                        etEmail.setError("Enter valid Email");
                        return;
                    } else if (!rbMale.isChecked() && !rbFemale.isChecked()) {
                        Toast.makeText(getApplicationContext(), "Select Gender", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (!etConfPassword.getText().toString().equals(etPassword.getText().toString())) {

                        etConfPassword.setError("Check Password");
                        return;
                    }
                    if (etMobile.getText().toString().length() != 10) {
                        etMobile.setError("Enter valid Mobile Number");
                        return;
                    }
                    if (!rbMale.isChecked() && !rbFemale.isChecked()) {
                        Toast.makeText(getApplicationContext(), "Select Gender", Toast.LENGTH_SHORT).show();
                    } else {

                        image_name = etEmail.getText().toString() + ".jpg";
                        if (bm == null) {
                            insertion();
                        } else {
                            new Encode_image().execute();
                        }

                    }


/*
              *//*  else if(!terms.isChecked())
                {*//*
                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(v,"Agree to Terms & Conditions !",Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Agree to Terms & Conditions !",Toast.LENGTH_LONG).show();
                    }
              //  }*/

                }

        });

    }

    public void setImage() {
        final CharSequence[] items = {"Click Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Click Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                    hasPermissionInManifest(getApplicationContext(),MediaStore.ACTION_IMAGE_CAPTURE);


                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                  //  hasPermissionInManifest(getApplicationContext(),);

                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        bm = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btnCapturePicture.setImageBitmap(bm);

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        btnCapturePicture.setImageBitmap(bm);

    }


    private class Encode_image extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // bitmap = BitmapFactory.decodeFile(file_uri.getPath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            //   Picasso.with(getApplicationContext()).load(fileUri.getPath()).resize(100,stream);
            byte[] array = stream.toByteArray();
            encoded_string = Base64.encodeToString(array, 0);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            makeRequest();
        }


        private void makeRequest() {
            // RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            pd.show();
            StringRequest request = new StringRequest(Request.Method.POST, Config.setImage, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject obj = new JSONObject(response);
                        int result = obj.getInt("success");
                        if (result == 1)
                        {

                            Toast.makeText(getApplicationContext(), "Registration Successful", Toast.LENGTH_LONG).show();
                            insertion();
                            pd.dismiss();
                        }
                        else{
                            insertion();
                            pd.dismiss();
                        }


                    }catch (Exception e){
                        pd.dismiss();
                        Toast.makeText(getApplicationContext(),"Failed t0 upload Image",Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {

                public void onErrorResponse(VolleyError error) {
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), "Check Your Internet Connection!", Toast.LENGTH_LONG).show();
                }

            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("encoded_string", encoded_string);
                    map.put("image_name", image_name);
                    return map;
                }


            };

            MyApplication.getInstance().addToReqQueue(request);
        }

    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

        public void insertion() {

        pd.show();
        String url = Config.url;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");
                    if (result == 1) {
                        //  new Encode_image().doInBackground();
                        Toast.makeText(getApplicationContext(), "Registration Successfull !" + result, Toast.LENGTH_SHORT).show();

                        Intent d = new Intent(RegistrationActivity.this, LoginActivity.class);
                        startActivity(d);
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(),"Failed to Register ! Please Try Again." + error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", etName.getText().toString());
                params.put("email", etEmail.getText().toString());
                params.put("password", etPassword.getText().toString());
                params.put("gender", rbgender.getText().toString());
                params.put("age", etAge.getText().toString());
                params.put("mobile", etMobile.getText().toString());
                params.put("address", etAddress.getText().toString());
                params.put("city", tvCity.getText().toString());
                params.put("reg_id", reg_id.getText().toString());
                return params;
            }
        };

     /*   int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
*/

        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);

    }

    @Override
    public void setText(String city) {
        tvCity.setText(city);
    }


    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }
}
