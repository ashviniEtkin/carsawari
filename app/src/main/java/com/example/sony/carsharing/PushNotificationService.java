package com.example.sony.carsharing;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by kundan on 10/22/2015.
 */
public class PushNotificationService extends GcmListenerService {

    public static int notify_no = 0;

    String name;
    SharedPreferences preferences;
    Intent intent;
    String offer_id;
    ContentValues v;
    String user_name, mobile_no, email_id,un;


    @Override
    public void onMessageReceived(String from, Bundle data) {

        preferences = getSharedPreferences(RideInfo.MyPREFERENCE, Context.MODE_PRIVATE);
         // = preferences.getString("b", LoginActivity.imgName);
        un  = preferences.getString("U_NAME", RideInfo.nm);

        offer_id = data.getString("offer_id");
        user_name = data.getString("user_name");
        mobile_no = data.getString("mobile_no");
        email_id = data.getString("email_id");


        createNotification();

    }

    public void createNotification() {


        intent = new Intent(getApplicationContext(), BookActivity.class);

        intent.putExtra("OFFER_ID", offer_id);
        intent.putExtra("USER_INFORMATION",user_name);//MOBILE_NO
        intent.putExtra("MOBILE_NO", mobile_no);
        intent.putExtra("EMAIL_ID",email_id);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(this, notify_no, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = {0, 1000, 1000};


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.sharing_car_one).setContentTitle("New Request For Car Sawari Booking")
                .setContentText(un + "  wants to book your ride !").setSound(soundUri).setVibrate(pattern)
                .setAutoCancel(true).getNotification();


        //	notificationManager.notify(notifyId,builder.build());

       /* Intent intentConfirm = new Intent();
        intentConfirm.setAction("Accept");
        PendingIntent pinConfirm = PendingIntent.getBroadcast(this, 0, intentConfirm, 0);
        builder.addAction(R.mipmap.accept, "Accept", pinConfirm);


        Intent intentCancel = new Intent();
        intentConfirm.setAction("Decline");
        PendingIntent pinCancel = PendingIntent.getBroadcast(this, 0, intentCancel, 0);
        builder.addAction(R.mipmap.decline, "Decline", pinCancel);*/
        //    builder.setAutoCancel(true);


    /*	Notification noti = new Notification.Builder(this)
                .setSmallIcon(R.drawable.sharing_car_one)
				.setTicker("Ticker Title")
				.setContentTitle("Car Sawari Booking")
				.setContentText(LoginActivity.name + "wants to book your ride !")
				.setSound(soundUri).setVibrate(pattern)
				.setContentIntent(pIntent).getNotification();


		noti.flags=Notification.FLAG_AUTO_CANCEL;

*/

        builder.setContentIntent(pIntent);
        notificationManager.notify(notify_no + 2, builder.build());

    }
}