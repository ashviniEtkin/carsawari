package com.example.sony.carsharing;


import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentStates extends Fragment
{
    TextView tvStates;
    Context ctx;
    Resources resources;
    String[] States;
    StateListAdapter listAdapter;
    ListView lvStates;
    FragmentManager manager;
    FragmentTransaction transaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_states,container,false);
        tvStates = (TextView)v.findViewById(R.id.tvStates);
        lvStates=(ListView)v.findViewById(R.id.lvStates);

        ctx=getActivity();
        resources = getResources();
        States = resources.getStringArray(R.array.states);

        listAdapter = new StateListAdapter(ctx,States);
        lvStates.setAdapter(listAdapter);



        lvStates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                FragmentCity city = new FragmentCity();
                city.setData(position,getActivity());
                manager = getActivity().getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.contain, city, "city");
                transaction.addToBackStack("city");
                transaction.commit();


            }
        });

        return v;
    }
}
