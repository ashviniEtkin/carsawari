package com.example.sony.carsharing;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 1/16/2016.
 */
public class CustomAdaptor extends BaseAdapter {

  //  int flag=0;
    Context ctx;
    List<UserInfo> list;
    public CustomAdaptor(Context ctx,List<UserInfo> list)
    {
        this.ctx=ctx;
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView==null)
        {
            holder= new ViewHolder();
            LayoutInflater inflater= (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.listview,null);
            convertView.setTag(holder);

            holder.tv1=(TextView)convertView.findViewById(R.id.user_name);
            holder.tv2=(TextView)convertView.findViewById(R.id.user_email);
            holder.tv3=(TextView)convertView.findViewById(R.id.user_password);
            holder.tv4=(TextView)convertView.findViewById(R.id.user_gender);
            holder.tv5=(TextView)convertView.findViewById(R.id.user_mobile);
            holder.tv6=(TextView)convertView.findViewById(R.id.user_address);
            holder.tv7=(TextView)convertView.findViewById(R.id.user_city);


        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }
        UserInfo objemp=  list.get(position);
        holder.tv1.setText(""+objemp.getName());
        holder.tv2.setText(""+objemp.getEmail());
        holder.tv3.setText(""+objemp.getPassword());
        holder.tv4.setText(""+objemp.getGender());
        holder.tv4.setText(""+objemp.getMobile());
        holder.tv5.setText(""+objemp.getAddress());
        holder.tv6.setText(""+objemp.getCity());

        return convertView;
    }


   /* private void update(final String s1, final String s2)
    {
        String url=Config.urlupdate;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      //  pd.dismiss();
                        try {


                            JSONObject jsonObject=new JSONObject(response);
                            // JSONArray jsonArray= new JSONArray(response);
                            // JSONObject jsonObject=jsonArray.getJSONObject(1);
                            int result=jsonObject.getInt("success");


                        } catch (Exception e) {
                            Toast.makeText(ctx,
                                    "Error",
                                    Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  pd.dismiss();
                Toast.makeText(ctx,
                        "failed to update"+error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String,String>();
             //   if((params.get("flag")).toString()=="0"&&(params.get("name")==s1)&&(params.get("password")==s2))
                params.put("flag", "1");
                params.put("name",s1);
                params.put("password",s2);
                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);
    }

    private void disable(final  String s3,final String s4){
        String url=Config.urlupdate;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //  pd.dismiss();
                        try {


                            JSONObject jsonObject=new JSONObject(response);
                            // JSONArray jsonArray= new JSONArray(response);
                            // JSONObject jsonObject=jsonArray.getJSONObject(1);
                            int result=jsonObject.getInt("success");


                        } catch (Exception e) {
                            Toast.makeText(ctx,
                                    "Error",
                                    Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  pd.dismiss();
                Toast.makeText(ctx,
                        "failed to update"+error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String,String>();
                //   if((params.get("flag")).toString()=="0"&&(params.get("name")==s1)&&(params.get("password")==s2))
                params.put("flag", "0");
                params.put("name",s3);
                params.put("password",s4);
                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);

    }*/

public  class  ViewHolder
    {
        TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7;

    }
}

