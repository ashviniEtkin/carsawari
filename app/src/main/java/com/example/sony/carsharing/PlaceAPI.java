package com.example.sony.carsharing;

import android.util.Log;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Lenovo on 2/9/2016.
 */
public class PlaceAPI {

    private static final String TAG = PlaceAPI.class.getSimpleName();
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBbmDAEriT0dZVUJ-ucvQFqCvkfm4_1oUg";

    // private static final String PLACE_ID = PlacesAutoCompleteAdapter.placeId;


    public ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //  sb.append("&types=(cities)");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }





}

/*

    public ArrayList<String> getPlaceId (String input)
    {
        ArrayList<String> placeIdList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
           // sb.append("&types=(cities)");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return placeIdList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return placeIdList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            placeIdList = new ArrayList<String>(predsJsonArray.length());
            //placeIdList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                placeIdList.add(predsJsonArray.getJSONObject(i).getString("place_id"));

            }
           // placeIdList = getPlaceId(input);
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return placeIdList;
    }



*/

    /*Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId).setResultCallback(new ResultCallback<PlaceBuffer>()
     {
        @Override
        public void onResult(PlaceBuffer places)
                     if (places.getStatus().isSuccess() && places.getCount() > 0)
             {
                final Place myPlace = places.get(0);
                Log.i(TAG, "Place found: " + myPlace.getName());
            }
            else
             {
                Log.e(TAG, "Place not found");
            }
            places.release();
        }
    });*/

/*


    public String[] getLatLng ()
    {

       // https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJrTLr-GyuEmsRBfy61i59si0&key=YOUR_API_KEY


        String[] LatLng={};
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE+"/details"+ OUT_JSON+"?");
            sb.append("placeid=" +PlacesAutoCompleteAdapter.placeId);
            sb.append("&key=" + API_KEY);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1)
            {
                jsonResults.append(buff, 0, read);
            }
        }
        catch (MalformedURLException e)
        {
            Log.e(TAG, "Error processing Places API URL", e);
            return LatLng;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return LatLng;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("html_attributions");

            // Extract the Place descriptions from the results
          //  LatLng = new String(predsJsonArray.length());
            //placeIdList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                LatLng[0]=predsJsonArray.getJSONObject(i).getString("lat");
                LatLng[1]=predsJsonArray.getJSONObject(i).getString("lng");

            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return LatLng;
    }

*/






  /*  public int getDistance(String loc1, String loc2)
    {
        // http://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&sensor=false

        int distance;

        ArrayList<String> resultList;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();


        try {
            StringBuilder sb = new StringBuilder("http://maps.googleapis.com/maps/api/directions/json?origin="+loc1+"&destination="+loc2+"&sensor=false");

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());


            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1)
            {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return distance;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return distance;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("geocoded_waypoints");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {

                ArrayList<String> arr=new ArrayList<String>();
              // JSONArray as= arr.add(predsJsonArray.getJSONObject(i).getString("legs"));

                JSONArray as1=jsonObj.getJSONArray("legs");
                for (int j=0;j<as1.length();j++){
                    arr.add(as1.getJSONObject(j).getString("distance"));

                }

            }



        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

            return distance;

    }

*/

