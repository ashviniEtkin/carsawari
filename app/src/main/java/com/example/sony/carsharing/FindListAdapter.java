package com.example.sony.carsharing;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SONY on 05/02/2016.
 */
public class FindListAdapter extends BaseAdapter
{


    List<FindPojo> list=new ArrayList<FindPojo>();
    Context ctx;

    public FindListAdapter(Context ctx,List<FindPojo> list){
        this.list=list;
        this.ctx=ctx;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final FindViewHolder fvholder;

        if(convertView==null){
            fvholder=new FindViewHolder();
            LayoutInflater inflater=(LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.listview_for_find_ride,parent,false);
            fvholder.date=(TextView)convertView.findViewById(R.id.list_find_ride_date);
            fvholder.time=(TextView)convertView.findViewById(R.id.list_find_ride_time);
            fvholder.src=(TextView)convertView.findViewById(R.id.list_find_src);
            fvholder.dest=(TextView)convertView.findViewById(R.id.list_find_dest);
            fvholder.seat_avail=(TextView)convertView.findViewById(R.id.list_find_seats_avail);
            fvholder.name=(TextView)convertView.findViewById(R.id.list_find_user_name);
            fvholder.age=(TextView)convertView.findViewById(R.id.list_find_user_age);
            fvholder.price=(TextView)convertView.findViewById(R.id.list_price);
            fvholder.offer_id=(TextView)convertView.findViewById(R.id.offer_id);
            fvholder.img=(ImageView)convertView.findViewById(R.id.list_find_Photo);
            fvholder.facility=(TextView)convertView.findViewById(R.id.list_find_facility);
            fvholder.car_name=(TextView)convertView.findViewById(R.id.list_find_car_name);
            fvholder.type=(TextView)convertView.findViewById(R.id.list_find_type);
            fvholder.music=(TextView)convertView.findViewById(R.id.list_find_music);
            fvholder.drink=(TextView)convertView.findViewById(R.id.list_find_drink);
            fvholder.smoke=(TextView)convertView.findViewById(R.id.list_find_smoke);
            fvholder.food=(TextView)convertView.findViewById(R.id.list_find_food);



            convertView.setTag(fvholder);
        }
        else
        {
            fvholder = (FindViewHolder) convertView.getTag();
        }
        FindPojo obj=list.get(position);
        fvholder.date.setText("DATE : "+obj.getDepart_date());
        fvholder.time.setText("TIME : "+obj.getDepart_time());
        fvholder.src.setText(obj.getSrc());
        fvholder.dest.setText(obj.getDest());
        fvholder.seat_avail.setText("AVAILABLE SEATS : "+obj.getSeat_avail());
        fvholder.price.setText("Rs." + obj.getPrice() + "/seat");
        fvholder.name.setText(obj.getName());
        fvholder.offer_id.setText(obj.getOffer_id());
        fvholder.offer_id.setVisibility(View.GONE);
        fvholder.age.setText(obj.getAge() + "Yr Old");

        fvholder.facility.setText("Facility : "+obj.getFacility());
        fvholder.car_name.setText("Car Name :"+obj.getCar_name());
        fvholder.type.setText("Type : "+obj.getType());

        //facility,car_name,type,music,drink,smoke,food;


        fvholder.music.setText(obj.getMusic());
        fvholder.drink.setText(obj.getDrink());
        fvholder.smoke.setText(obj.getSmoke());
        fvholder.food.setText(obj.getFood());

       if(fvholder.music.getText().toString().equals("1")){
           fvholder.music.setVisibility(View.VISIBLE);
       }
        else{
           fvholder.music.setVisibility(View.GONE);
       }

        if(fvholder.drink.getText().toString().equals("1")){
            fvholder.drink.setVisibility(View.VISIBLE);
        }
        else{
            fvholder.drink.setVisibility(View.GONE);
        }

        if(fvholder.smoke.getText().toString().equals("1")){
            fvholder.smoke.setVisibility(View.VISIBLE);
        }
        else{
            fvholder.smoke.setVisibility(View.GONE);
        }

        if(fvholder.food.getText().toString().equals("1")){
            fvholder.food.setVisibility(View.VISIBLE);
        }
        else{
            fvholder.food.setVisibility(View.GONE);
        }




        Picasso.with(ctx).load(Config.IMAGE_DIRECTORY_NAME+obj.getEmail()+".jpg").resize(100,100).into(fvholder.img);
      //  RequestQueue requestQueue = Volley.newRequestQueue(ctx);
      //  requestQueue.add()

        return convertView;
    }
    public  class FindViewHolder
    {

        TextView date, time, src, dest, seat_avail,price,name,age,offer_id,facility,car_name,type;
        TextView music,drink,smoke,food;
        ImageView img;

    }
}
