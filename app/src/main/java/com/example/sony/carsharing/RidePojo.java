package com.example.sony.carsharing;

import android.content.Context;

/**
 * Created by SONY on 05/02/2016.
 */
public class RidePojo {

    String depart_date, depart_time, return_date, return_time, src, dest, price, seat_avail,comment,offer_id;
    int id;

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    RidePojo(String offer_id,String src, String dest,String date, String time,  String seat_avail,String price,String comment) {
        this.offer_id=offer_id;
        this.price=price;
        this.comment=comment;
        depart_date = date;
        depart_time = time;
        this.src = src;
        this.dest = dest;
        this.seat_avail = seat_avail;

    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    RidePojo(String src, String dest,String date, String time, String rdate, String rtime,  String seat_avails) {
        depart_date = date;
        depart_time = time;
        return_date = rdate;
        return_time = rtime;
        this.src = src;
        this.dest = dest;
        this.seat_avail = seat_avails;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReturn_time() {
        return return_time;
    }

    public void setReturn_time(String return_time) {
        this.return_time = return_time;
    }

    public String getDepart_date() {
        return depart_date;
    }

    public void setDepart_date(String depart_date) {
        this.depart_date = depart_date;
    }

    public String getDepart_time() {
        return depart_time;
    }

    public void setDepart_time(String depart_time) {
        this.depart_time = depart_time;
    }

    public String getReturn_date() {
        return return_date;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSeat_avail() {
        return seat_avail;
    }

    public void setSeat_avail(String seat_avail) {
        this.seat_avail = seat_avail;
    }


}
