package com.example.sony.carsharing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {

    EditText tv_email;
    SharedPreferences preference, pref1, pref;
    SharedPreferences.Editor editor;
    public static String email1, pass1, name;
    public static String MyPREFERENCE = "MyPref";
    public static final String MyPREFERENCES = "MyPrefs";
    Intent l1,l2;
    EditText et_password;
    Button sign_in, btnSign_up;
    ProgressDialog pd;
    public static String imgName,reg_id,mobile;
    TextView forget_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tv_email = (EditText) findViewById(R.id.email);
        et_password = (EditText) findViewById(R.id.password);
        sign_in = (Button) findViewById(R.id.btn_sign);
        btnSign_up = (Button) findViewById(R.id.btnSign_up);
        forget_password=(TextView)findViewById(R.id.forget_password);

        forget_password.setText(Html.fromHtml("<a href='#'> Forget Password</a>"));
        l1 = new Intent(LoginActivity.this, HomeActivity.class);
        l2= new Intent(LoginActivity.this, ForgetPassword.class);


        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(l2);

            }
        });

        btnSign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);

            }
        });

        preference = PreferenceManager.getDefaultSharedPreferences(this);
        int result = preference.getInt("a", 0);
        if (result == 1) {

            startActivity(l1);
        }

        email1 = tv_email.getText().toString();
        pass1 = et_password.getText().toString();
        pd = new ProgressDialog(this);
        pd.setTitle("Please wait...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pref1 = getSharedPreferences(MyPREFERENCE, Context.MODE_PRIVATE);
                editor = pref1.edit();
                editor.putString("EMAIL", tv_email.getText().toString());
                editor.commit();

                if (TextUtils.isEmpty(tv_email.getText().toString().trim())) {
                    tv_email.setError("Enter Username");
                    return;
                } else if (TextUtils.isEmpty(et_password.getText().toString().trim())) {
                    et_password.setError("Enter password");
                    return;
                } else {

                    imgName = Config.IMAGE_DIRECTORY_NAME + tv_email.getText().toString() + ".jpg";
                    preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    editor = preference.edit();
                    editor.putString("b", imgName);
                    editor.commit();
                    email1 = tv_email.getText().toString();
                    pass1 = et_password.getText().toString();

                    // forHeaderName();
                    // showHeaderItem();
                    campare(v);


                }
            }
        });


    }

    public void campare(final View view) {


        String url = Config.urltv;
        pd.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject obj = new JSONObject(response);
                    int result = obj.getInt("result");
                    if (result == 1) {
                        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("a", 1);
                        editor.commit();
                        pd.dismiss();
                        showHeaderItem(view);
                        // startActivity(l1);
                    }
                    else
                    {

                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view, "Wrong Username or Password !", Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Wrong Username or Password !", Toast.LENGTH_LONG).show();
                        }

                        pd.dismiss();

                    }

                } catch (Exception e)
                {
                    pd.dismiss();

                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(view, "Login Failed ! Please Try Again.", Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Login Failed ! Please Try Again.", Toast.LENGTH_SHORT).show();
                    }


                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                pd.dismiss();

                if (Build.VERSION.SDK_INT > 20)
                {
                    Snackbar.make(view, "Check Your Internet Connection !", Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Check Your Internet Connection !" + error.toString(), Toast.LENGTH_SHORT).show();
                }


            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("email", tv_email.getText().toString());
                params.put("password", et_password.getText().toString());
                return params;
            }
        };


        MyApplication.getInstance().addToReqQueue(postRequest);
        //   this.finish();
    }

    public void showHeaderItem(final View view) {

        String url = Config.headerImg;
        // final Intent i1 = new Intent(LoginActivity.this, HomeActivity.class);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject obj = new JSONObject(response);
                    int result = obj.getInt("result");
                    if (result == 1) {

                        forHeaderName(view);
                        // startActivity(l1);
                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view, "Login Successful", Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_SHORT).show();
                        }


                    } else
                    {
                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view, "Failed to Upload ! Please Try Again.", Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Failed to Upload ! Please Try Again.", Toast.LENGTH_LONG).show();
                        }


                          forHeaderName(view);
                    }
                    //   continue;
                } catch (Exception e)

                {
                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(view, "Failed to Upload ! Please Try Again.", Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Failed to Upload ! Please Try Again.", Toast.LENGTH_SHORT).show();
                    }

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                //  pd.dismiss();

                if (Build.VERSION.SDK_INT > 20)
                {
                    Snackbar.make(view, "Check Your Internet Connection!", Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Check Your Internet Connection!" + error.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "text/plain");
                params.put("name", tv_email.getText().toString() + ".jpg");
                return params;
            }
        };


        MyApplication.getInstance().addToReqQueue(postRequest);
    }


    public void forHeaderName(final View view) {

        String url = Config.forHeaderName;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject j1 = new JSONObject(response);
                    JSONArray result = j1.getJSONArray("result");
                    JSONObject obj = result.getJSONObject(0);
                    name = obj.getString("name");
                    reg_id = obj.getString("reg_id");
                    mobile = obj.getString("mobile");

                    preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    editor = preference.edit();
                    editor.putString("name", name);
                    editor.putString("reg_id",reg_id);
                    editor.putString("mobile",mobile);
                    editor.commit();
                    startActivity(l1);

                } catch (Exception e)
                {

                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(view,e.toString(), Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }


                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view,error.toString(), Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("email", tv_email.getText().toString());
                params.put("password", et_password.getText().toString());
                return params;
            }
        };


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);
    }

}


