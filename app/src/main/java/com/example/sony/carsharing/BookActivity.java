package com.example.sony.carsharing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BookActivity extends AppCompatActivity {

    TextView user_information;
    Button book_ride,reject_ride;
    Intent in1;
    String[] arr;

    public  static  String oFF_ID,mobileNO,user_name,emailID;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_book);
        book_ride=(Button)findViewById(R.id.bk);
        reject_ride=(Button)findViewById(R.id.reject);
        user_information=(TextView)findViewById(R.id.user_information);


      //  b=

        oFF_ID=getIntent().getExtras().getString("OFFER_ID");
        mobileNO=getIntent().getExtras().getString("MOBILE_NO");
        user_name=getIntent().getExtras().getString("USER_INFORMATION");
        emailID=getIntent().getExtras().getString("EMAIL_ID");


     user_information.setText(user_name +"  want to book Your Ride");
        book_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SmsManager sm = SmsManager.getDefault();
                sm.sendTextMessage(mobileNO, null, "Congratulations ! Your Booking is Approved and his name id is "+emailID, null, null);
                Toast.makeText(getApplicationContext(), "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                book_ride();
                finish();
            }
        });
        reject_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SmsManager sm = SmsManager.getDefault();
                sm.sendTextMessage(mobileNO, null, "Sorry! Booking Request Rejected.Please Try Again.", null, null);
                Toast.makeText(getApplicationContext(), "SMS Sent Successfully", Toast.LENGTH_SHORT).show();
                finish();

            }
        });


    }

    public void book_ride(){

        String url = Config.booking;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int result = jsonObject.getInt("success");
                    if (result == 1)
                    {
                        Toast.makeText(getApplicationContext(), "Booking Confirmed", Toast.LENGTH_SHORT).show();
                        //startWakefulService(context, in);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Please Check Your Internet Connection !", Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("offer_id", oFF_ID);
                return params;
            }
        };

        MyApplication.getInstance().addToReqQueue(postRequest);


    }
}
