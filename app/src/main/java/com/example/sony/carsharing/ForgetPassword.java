package com.example.sony.carsharing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassword extends AppCompatActivity {

    EditText mobile_no;
    Button btn_forget_pass;
    Intent intentt;
    String email, password, name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        mobile_no = (EditText) findViewById(R.id.forget_password_send_Mobile_no);
        btn_forget_pass = (Button) findViewById(R.id.btn_forget_pass);

        intentt = new Intent(ForgetPassword.this, LoginActivity.class);

        btn_forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(mobile_no.getText().toString().trim())) {
                    mobile_no.setError("Enter Mobile Number");
                } else {
                    forget_password(mobile_no.getText().toString());
                }

            }
        });


    }

    public void forget_password(final String number) {

        String url = Config.forget_password;

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("result");
                    JSONObject obj = result.getJSONObject(0);

                    email = obj.getString("email");
                    password = obj.getString("password");
                    name = obj.getString("name");

                    SmsManager sm = SmsManager.getDefault();


                    sm.sendTextMessage(mobile_no.getText().toString(), null, "Dear " + name + " mam/sir, Your Email id is " + email + " and password is " + password, null, null);
                    Toast.makeText(getApplicationContext(), "SMS Sent Successfully", Toast.LENGTH_SHORT).show();


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("mobile", number);


                return params;
            }
        };

        MyApplication.getInstance().addToReqQueue(postRequest);
        this.finish();
    }

}
