package com.example.sony.carsharing;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Lenovo on 1/29/2016.
 */
public class CityListAdapter extends BaseAdapter
{
    Context ctx;
    String[] cities;

    public CityListAdapter(Context ctx, String[] cities)
    {
        this.ctx = ctx;
        this.cities = cities;
    }

    @Override
    public int getCount()
    {
        return cities.length;
    }

    @Override
    public Object getItem(int position) {
        return cities[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        ViewHolder holder = null;
        if (v == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.city_list,null);
            holder.tvCityName= (TextView)v.findViewById(R.id.tvCityName);
            v.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) v.getTag();
        }
        holder.tvCityName.setText(cities[position]);
        return v;
    }

    public class ViewHolder
    {
        TextView tvCityName;
    }
}
