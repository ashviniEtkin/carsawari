package com.example.sony.carsharing;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Lenovo on 2/1/2016.
 */
public class prefOperations {


    public static void saveUser(UserInfo user, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, user.id + "", 0);
        complexPreferences.putObject(user.id+ "", user);
        complexPreferences.commit();
        complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "id", 0);
        complexPreferences.putObject("id", user.id + "");
        complexPreferences.commit();
    }


    public static void delete(String id, Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, id, 0);
        complexPreferences.remove(id);
        complexPreferences.commit();

    }


    public static UserInfo getUser(Context ctx,String id) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, id, 0);
        UserInfo user = complexPreferences.getObject(id, UserInfo.class);
        return user;
    }


    public static int getId(Context ctx) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "id", 0);
        String id = complexPreferences.getObject("id", String.class);
        if(id==null)
            id="-1";
        return Integer.parseInt(id)+1;
    }

    public static ArrayList<UserInfo> getUsers(Context context){
        ArrayList<UserInfo> users=new ArrayList<UserInfo>();
        for(int i=0;i<getId(context);i++){
            UserInfo re=getUser(context,i+"");
            if(re!=null)
                users.add(re);

        }
        return users;
    }




}