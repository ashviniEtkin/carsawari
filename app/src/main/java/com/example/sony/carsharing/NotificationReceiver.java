package com.example.sony.carsharing;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2/25/2016.
 */
public class NotificationReceiver extends WakefulBroadcastReceiver {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static String mobile, offerId;

    Intent in;

    @Override
    public void onReceive(final Context context, Intent intent) {

        preferences = context.getSharedPreferences(RideInfo.MyPREFERENCE, Context.MODE_PRIVATE);
        mobile = preferences.getString("mobileNo", RideInfo.mobileNo);
        offerId = preferences.getString("offer_id", RideInfo.oId);

        if (intent.getAction().equals("Accept")) {
          //  in = new Intent(context, BookActivity.class);


            SmsManager sm = SmsManager.getDefault();
            sm.sendTextMessage(mobile, null, "Congratulations ! Your Booking is Approved.", null, null);
            Toast.makeText(context, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();

  /*          String url = Config.booking;
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int result = jsonObject.getInt("success");
                        if (result == 1)
                        {
                            Toast.makeText(context, "Booking Confirmed", Toast.LENGTH_SHORT).show();
                            startWakefulService(context, in);

                        }
                        else
                        {
                            Toast.makeText(context, "Please Check Your Internet Connection !", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                }
            })

            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("offer_id", offerId);
                    return params;
                }
            };

            MyApplication.getInstance().addToReqQueue(postRequest);
*/
        } else if (intent.getAction().equals("Decline")) {
            SmsManager sm = SmsManager.getDefault();
            sm.sendTextMessage(mobile, null, "Sorry! Booking Request Rejected.Please Try Again.", null, null);
            Toast.makeText(context, "SMS Sent Successfully", Toast.LENGTH_SHORT).show();

        }

    }

}



