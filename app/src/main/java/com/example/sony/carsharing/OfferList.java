package com.example.sony.carsharing;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class OfferList extends Fragment
{

    ListView lview;
    SharedPreferences pref;
    List<RidePojo> arr = new ArrayList<RidePojo>();
    public static  String eid;

    public OfferList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_offer_list, container, false);
        lview = (ListView) v.findViewById(R.id.offer_list);
        OfferListAdapter offerListAdapter = new OfferListAdapter(getActivity(),getFragmentManager(),OfferRideFragment.arr);
        lview.setAdapter(offerListAdapter);
        offerListAdapter.notifyDataSetChanged();


        pref = getActivity().getSharedPreferences(LoginActivity.MyPREFERENCE, Context.MODE_PRIVATE);
        eid = pref.getString("EMAIL", LoginActivity.email1);

        twoWayRideShowData(v);
        return v;
    }

    public void twoWayRideShowData(final View view)
    {
        String url = Config.twoWayRide;
        //    pd.show();
                 StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
                 {
                  @Override
                    public void onResponse(String response)
                  {
                // pd.dismiss();
                        try
                        {
                            /*JSONObject j1 = new JSONObject(response);
                            JSONArray result = j1.getJSONArray("result");
                            JSONObject obj = result.getJSONObject(0);*/
                            JSONObject j1 = new JSONObject(response);
                            JSONArray result = j1.getJSONArray("result");
                          for (int i = 0; i < result.length(); i++)
                          {
                              JSONObject obj = result.getJSONObject(i);
                              String offer_id=obj.getString("offer_id");
                              String from_location = obj.getString("from_location");
                              String to_location = obj.getString("to_location");
                              String depart_date = obj.getString("depart_date");
                              String depart_time = obj.getString("depart_time");
                              String avail_seat = obj.getString("avail_seat");
                              String price= obj.getString("price");
                              String comment=obj.getString("comment");
                              RidePojo objuser = new RidePojo(offer_id,from_location, to_location, depart_date, depart_time, avail_seat,price,comment);
                              arr.add(objuser);
                          }
                          OfferListAdapter offer = new OfferListAdapter(getActivity(), getActivity().getSupportFragmentManager(), arr);
                          lview.setAdapter(offer);

                        }
                        catch (Exception e)
                        {
                            if (Build.VERSION.SDK_INT > 20)
                            {
                                Snackbar.make(view, "Error Loading Data !", Snackbar.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "Error Loading Data !", Toast.LENGTH_LONG).show();
                            }

                            e.printStackTrace();
                        }
                  }
                 },
                 new Response.ErrorListener()
                 {
                     @Override
                     public void onErrorResponse(VolleyError error)
                     {
                         //  pd.dismiss();
                         // Show timeout error message
                         if (Build.VERSION.SDK_INT > 20)
                         {
                             Snackbar.make(view, error.toString(), Snackbar.LENGTH_LONG).show();
                         }
                         else
                         {
                             Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                         }

                     }
                }) {
                     protected Map<String, String> getParams() {
                         Map<String, String> params = new HashMap<String, String>();
                         params.put("Content-type", "text/plain");
                         params.put("email", eid);
                         return params;
                     }
                 };
        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        // Adding request to request queue
        MyApplication.getInstance().addToReqQueue(postRequest);
    }



}

