package com.example.sony.carsharing;

/**
 * Created by SONY on 16/01/2016.
 */
public class UserInfo {
    public int mobile;
    public String name;
    public String email;
    public String password;
    public String address;
    public String city;
    public String gender;
    public String flag;
    public  int id;
    UserInfo(String name, String password)
    {
        this.name=name;
        this.password=password;
    }
     UserInfo(String name, String email, String password, String gender,int mobile, String address, String city){

       this.mobile=mobile;
        this.name=name;
        this.email=email;
       this.gender=gender;
        this.password=password;
        this.address=address;
        this.city=city;

    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFlag() {
        return flag;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
