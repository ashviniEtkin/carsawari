package com.example.sony.carsharing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SONY on 05/02/2016.
 */
public class OfferListAdapter extends BaseAdapter {

    /*  SharedPreferences pref;
      SharedPreferences.Editor editor;
      public static  String dest,src,d_time,d_date
  */
    FragmentManager manager;
    FragmentTransaction transaction;
    public String from_location, to_location, depart_date, depart_time, price;
    public static String offer_id;
    List<RidePojo> list = new ArrayList<RidePojo>();
    Context ctx;
    public static String src, dest, date, time, prc, c;

    public OfferListAdapter(Context ctx, FragmentManager manager, List<RidePojo> list) {
        this.list = list;
        this.ctx = ctx;
        this.manager = manager;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final OfferViewHolder ovholder;

        if (convertView == null) {
            ovholder = new OfferViewHolder();
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_for_offer_ride, parent, false);
            ovholder.date = (TextView) convertView.findViewById(R.id.list_offer_ride_date);
            ovholder.time = (TextView) convertView.findViewById(R.id.list_offer_ride_time);
            ovholder.src = (TextView) convertView.findViewById(R.id.list_src);
            ovholder.dest = (TextView) convertView.findViewById(R.id.list_dest);
            ovholder.seat_avail = (TextView) convertView.findViewById(R.id.list_seats_avail);
            ovholder.price = (TextView) convertView.findViewById(R.id.list_price);
            ovholder.comment = (TextView) convertView.findViewById(R.id.list_comment);
            ovholder.offer_id = (TextView) convertView.findViewById(R.id.list_offer_id);
            ovholder.edit = (Button) convertView.findViewById(R.id.list_btn_edit);
            ovholder.delete = (Button) convertView.findViewById(R.id.list_btn_delete);
            convertView.setTag(ovholder);
        } else {
            ovholder = (OfferViewHolder) convertView.getTag();
        }
        RidePojo obj = list.get(position);
        ovholder.date.setText(obj.getDepart_date());
        ovholder.time.setText(obj.getDepart_time());
        ovholder.src.setText(obj.getSrc());
        ovholder.dest.setText(obj.getDest());
        ovholder.seat_avail.setText("AVAILABLE SEATS : " + obj.getSeat_avail());
        ovholder.price.setText(obj.getPrice());
        ovholder.comment.setText(obj.getComment());
        ovholder.offer_id.setText(obj.getOffer_id());


        ovholder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                src = ovholder.src.getText().toString();
                dest = ovholder.dest.getText().toString();
                date = ovholder.date.getText().toString();
                time = ovholder.time.getText().toString();
                prc = ovholder.price.getText().toString();
                c = ovholder.comment.getText().toString();
                offer_id = ovholder.offer_id.getText().toString();


/*
                for(int i=0; i<manager.getBackStackEntryCount(); i++)
                {
                    manager.popBackStack();
                }*/

                EditOfferRideFragment efrag = new EditOfferRideFragment();
                transaction = manager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
                transaction.add(R.id.Homecontainer, efrag, "EditOfferRideFragment");
                transaction.addToBackStack("EditOfferRideFragment");
                transaction.commit();

            }
        });

        ovholder.delete.setTag(position);

        ovholder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String offer_id1 = ovholder.offer_id.getText().toString();
                int pos = (int) v.getTag();

                delete(offer_id1, pos, v);


            }
        });


        return convertView;
    }

    public class OfferViewHolder {

        TextView date, time, src, dest, seat_avail, price, comment, offer_id;
        Button edit, delete;
    }

    public void delete(final String offer_Id, final int pos, final View view) {

        String url = Config.delete;
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    int result = obj.getInt("success");

                    if (result == 1) {
                      /*  OfferListAdapter offerListAdapter = new OfferListAdapter(ctx, manager, list);
                        offerListAdapter.notifyDataSetChanged();*/


                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view, "Your Ride has been Canceled !", Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(ctx, "Your Ride has been Canceled !", Toast.LENGTH_LONG).show();
                        }


                        list.remove(pos);
                        notifyDataSetChanged();



                    } else
                    {
                        if (Build.VERSION.SDK_INT > 20)
                        {
                            Snackbar.make(view, "Sorry ! Cannot Delete.", Snackbar.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(ctx, "Sorry ! Cannot Delete.", Toast.LENGTH_LONG).show();
                        }



                    }

                } catch (Exception e)
                {

                    if (Build.VERSION.SDK_INT > 20)
                    {
                        Snackbar.make(view, "Please Try Again !", Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(ctx, "Please Try Again !", Toast.LENGTH_SHORT).show();
                    }



                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (Build.VERSION.SDK_INT > 20)
                {
                    Snackbar.make(view, "Check Your Internet Connection !", Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(ctx, "Check Your Internet Connection !" + error.toString(), Toast.LENGTH_SHORT).show();
                }

               
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("offer_id", offer_Id);

                return params;
            }
        };
        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(postRequest);

        MyApplication.getInstance().addToReqQueue(postRequest);

    }

}
