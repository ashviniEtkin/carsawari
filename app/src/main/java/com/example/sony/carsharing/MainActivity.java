package com.example.sony.carsharing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    TextView tvCar, tvSharing ,share,share_money,drive;
    ImageView iv;
    private static int SPLASH_TIME_OUT = 4500;
    SharedPreferences preferences;
    Animation anim1,anim2, anim3,anim4;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        iv = (ImageView) findViewById(R.id.car_image);
        iv.setImageResource(R.drawable.sharing_car_one);
        tvSharing = (TextView) findViewById(R.id.Car);
        tvCar = (TextView) findViewById(R.id.Car_Sharing);
        share=(TextView)findViewById(R.id.share);
        share_money=(TextView)findViewById(R.id.Share_Money);
        drive=(TextView)findViewById(R.id.drive);

       /* Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Aisha Script.ttf");
        tvSharing.setTypeface(face);
        tvCar.setTypeface(face);
        share_money.setTypeface(face);
        share.setTypeface(face);
        drive.setTypeface(face);
*/
        anim2 = AnimationUtils.loadAnimation(this, R.anim.blink1);
        anim1 = AnimationUtils.loadAnimation(this, R.anim.right_slide_in);
        anim4 = AnimationUtils.loadAnimation(this, R.anim.for_car);
        anim3 = AnimationUtils.loadAnimation(this, R.anim.translate);
        iv.startAnimation(anim4);
        tvSharing.startAnimation(anim1);
        tvCar.startAnimation(anim3);
        share.startAnimation(anim2);
        share_money.startAnimation(anim2);
        drive.startAnimation(anim2);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                int result = preferences.getInt("a", 0);
                if (result == 1) {
                    Intent l1 = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(l1);
                    finish();
                } else
                {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }

        }, SPLASH_TIME_OUT);



        turnGPSOn(); // method to turn on the GPS if its in off state.

    }



    public void turnGPSOn() {
        try {

            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", true);
            this.sendBroadcast(intent);

            String provider = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!provider.contains("gps"))
            { //if gps is disabled
                final Intent poke = new Intent();
                poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                poke.setData(Uri.parse("3"));
                this.sendBroadcast(poke);
            }
        } catch (Exception e) {

        }
    }

}
